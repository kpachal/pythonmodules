# Kate Pachal, Feb 2013
#
# This class helps the user submit jobs to the grid,
# determine when they have finished, download the output,
# and validate it as quickly and reliably as possible.
#
# Much of the structure stolen from the legendary
# Thomas Gillam, with thanks.


# Standard imports
import sys
import subprocess
import time
from os import environ, path
from pandatools import PdbUtils
from dq2object import Dq2Job
import readline

# Import pbook
tmp, __name__ = __name__, 'pbook'
execfile(path.join(environ["ATLAS_LOCAL_ROOT_BASE"], "x86_64/PandaClient/currentJedi/bin/pbook") )
__name__ = tmp

class Dq2Getter(object) :

  ## ----------------------------------------------------
  ## Initialisers
 
  def __init__(self) :
    self.commandList = []
    self.outputdir = "/data/atlas/atlasdata/pachal/dijets"
    self.defineDownloadAsStuck = 3600 # One hour
    self.downtime = 30 # Rest time in seconds
    self.dq2RetryLimit = 3
    self.maxDQ2Streams = 3
    self.nDQ2StreamsInProgress = 0
    self.dq2SetupScript = "/home/pachal/scripts/dq2_setup.sh"

    self.syncToRunningJobs = False
    self.additionalJobs = []

    self.createPbook()
    self.createPdbUtils()

  def createPbook(self) :
    enforceEnter = False
    verbose   = False
    restoreDB = False
    self.pbookCore = PBookCore(enforceEnter, verbose, restoreDB)

  def createPdbUtils(self) :
    verbose = False
    PdbUtils.initialzieDB(verbose)

  ## ----------------------------------------------------
  ## Setters

  # Specify all the prun commands you will use
  def setContainerList(self, containerList) :
    self.containerList = containerList

  # Specify name of your local directory where output will be sent
  def setOutputDirectory(self, outputdir) :
    self.outputdir = outputdir

  # Maximum number of subjobs dq2-getting things at once
  def setMaxDQ2Streams(self, maxStreams) :
    self.maxDQ2Streams = maxStreams

  # Time between checking status, in seconds
  def setDowntime(self, downtime) :
    self.downtime = downtime

  # Maximum amount of time to try to dq2-get before killing subprocess
  def setDownloadTimeout(self, timeout) :
    self.defineDownloadAsStuck = timeout

  # Setup script for dq2
  # NOTE: Don't change, if possible. Script also needs to set up new
  # version of python compatible with dq2.
  # Source: http://atlas-sw.cern.ch/cgi-bin/cvsweb.cgi/dq2.clientapi/ 
  # lib/dq2/clientapi/DQ2.py?rev=1.44;content-type=text%2Fplain;
  # cvsroot=atlas-dq2;only_with_tag=MAIN
  def setDQ2SetupScript(self, script) :
    self.dq2SetupScript = script

  # Do we re-download pre-existing datasets?
  def setDoNotReDownload(self, doNotReDownload=False) :
    self.doNotReDownload = doNotReDownload

  ## ----------------------------------------------------
  ## Main function

  def execute(self) :

    # Hold output jobs
    self.failedJobs = []
    self.missingDatasets = []
    self.successfulJobs = []

    # Run all commands to submit new jobsets to the grid.
    # Retrieve the JobIDs and stats of all newly created jobs.
    print "About to retrieve requested files"
    self.currentJobs = {}
    counter = 0
    for item in self.containerList :
      newdq2job = Dq2Job(item,counter,self.outputdir,self.dq2SetupScript,self.defineDownloadAsStuck,self.dq2RetryLimit,self.doNotReDownload)
      self.currentJobs[counter] = newdq2job
      counter = counter + 1
    print "Monitoring jobs",self.currentJobs.keys()
    
    # Run this until all jobs are complete.
    while len(self.currentJobs.keys())>0 :

      # Check each job and act accordingly.
      self.currentJobIDs = sorted(self.currentJobs.keys())
      for jobID in self.currentJobIDs :
        job = self.currentJobs[jobID]
        self.getOutput(job)

      # Wait required gap time, then go to next iteration.
      print "\n"
      time.sleep(self.downtime)

    print "All jobs finished."
    print "Successful jobs:",self.successfulJobs
    print "Failed jobs:",self.failedJobs
    print "Jobs with no downloadable data on grid:",self.missingDatasets

    sys.exit(0)

  ## ----------------------------------------------------
  ## Constituent functions

  def getOutput(self, job) :
    '''Start process of dq2-getting output from completed job'''
    if job.isCurrentlyDownloading :
      status = job.checkDownloadProgress()
      if status == 'incomplete' :
        print "Job",job.JobID,"currently downloading, status==incomplete"
        return
      else :
        print "Job",job.JobID,"done downloading!"
        del self.currentJobs[job.JobID]
        self.nDQ2StreamsInProgress -= 1
        if job.isDownloadFailed == True :
          if job.noDataSetError :
            self.missingDatasets.append(job.outDS)
          else :
            self.failedJobs.append(job.outDS)
        else :
          self.successfulJobs.append(job.outDS)
    elif self.nDQ2StreamsInProgress >= self.maxDQ2Streams :
      print "Holding job",job.JobID,"until dq2 stream available."
      return
    else :
      print "Starting download of job",job.JobID
      self.nDQ2StreamsInProgress += 1
      job.retrieveOutput()

