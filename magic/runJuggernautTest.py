#!/usr/bin/env python

# Create an instance of the tool - will require python
# setup script to have been called prior, so that my modules
# directory is in the path
import sys,os
import getopt
import datetime
import subprocess
from magic.juggernaut import Juggernaut
import ConfigParser

######################################
# Main function
def main(argv):

  outputDir = "/home/pachal/PythonModules/magic/test/"

  myJuggernaut = Juggernaut()
  myJuggernaut.setOutputDirectory(outputDir)
  myJuggernaut.setUseNewSite(True)
#  myJuggernaut.setMaxDQ2Streams(10)

  fullcommand = "prun --noBuild --exec 'echo Hello Kate > output.txt' --outDS=user.kpachal.helloworld --express --outputs=output.txt"

  # Step where I make tarball with current coide.
  commandList = [fullcommand]
  myJuggernaut.setCommandList(commandList)
#  jobslist = [12388,12389,12390,12392,12393,12394]
#  myJuggernaut.syncToSpecifiedJobs(jobslist)

  myJuggernaut.execute()

  print "Done."

######################################
if __name__ == "__main__":
   main(sys.argv[1:])

