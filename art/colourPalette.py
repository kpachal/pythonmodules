import ROOT

class ColourPalette(object) :

  ## ----------------------------------------------------
  ## Initialisers

  def __init__(self) :

    self.dataLineColor = ROOT.kBlack
    self.dataPointColor = ROOT.kBlack
    self.dataFillColor = ROOT.kWhite
    self.palette = None

  ## ----------------------------------------------------
  ## User-accessible functions

  def setColourPalette(self, colourPalette) :
    self.palette = colourPalette
    if self.palette == "ATLAS" :
      self.setATLASColours()
    elif self.palette == "Oxford" :
      self.setOxfordColours()
    elif self.palette == "Teals" :
      self.setTealColours()
    elif self.palette == "Tropical" :
      self.setTropicalColours()
    elif self.palette == "ExtendedTropical" :
      self.setExtendedTropicalColours()

  def getColourPalette(self) :
    return self.palette

  ## ----------------------------------------------------
  ## Individual palette definitions

  def setATLASColours(self) :

    # Plot fill colours
    self.oneFillColour = ROOT.kYellow
    self.oneFitColour = ROOT.kRed
    self.statisticaTestFillColour = ROOT.kYellow
    self.originalStatMarkerColour = ROOT.kBlue

    self.fitLineColor = ROOT.kRed

    # Stat plots
    self.statisticaTestFillColour = ROOT.kYellow
    self.statisticalTestArrowColour = ROOT.kBlue
    self.tomographyGraphColour = ROOT.kRed

    # Limit-setting plots
    self.oneSigmaBandColour = ROOT.kGreen#ROOT.kSpring
    self.twoSigmaBandColour = ROOT.kYellow
    self.signalLineColours = [ROOT.kBlue,ROOT.kAzure+7,ROOT.kTeal+9]
    self.signalErrorColours = [ROOT.kBlue-9,ROOT.kCyan-9,ROOT.kTeal+8]#ROOT.kAzure+6,ROOT.kTeal+8]

    self.shortGoodColours = self.mediumGoodColours
    self.defaultGoodColours = self.mediumGoodColours

  def setOxfordColours(self) :

    # Plot fill colours
    self.oneFillColour = ROOT.TColor(500,72,145,220)
    self.oneFitColour = ROOT.TColor(501,190,15,52)
    self.statisticaTestFillColour = ROOT.TColor(502,158,206,235)
    self.originalStatMarkerColour = ROOT.TColor(503,0,33,71)

    self.fitLineColor = ROOT.kRed

    # Limit-setting plots
    self.oneSigmaBandColour = ROOT.TColor(500)
    self.twoSigmaBandColour = ROOT.TColor(502)
    self.signalLineColour = ROOT.TColor(501)

  def setTealColours(self) :

    self.blue1 = ROOT.TColor(1010,0.833,1.00,0.967)
    self.blue2 = ROOT.TColor(1011,0.392,0.825,0.850)
    self.bluegreen = ROOT.TColor(1012,0.200,0.470,0.521)
    self.darkred = ROOT.TColor(1013,0.808,0.063,0.175)
    self.mauve = ROOT.TColor(1014,0.817,0.792,0.783)
#    self.mauve = ROOT.TColor(1004,178.0/255.0, 102.0/255.0, 255.0/255.0)
#    self.mauve = ROOT.TColor(1004,192.0/255.0, 192.0/255.0, 192.0/255.0)

    # Limit-setting plots
    self.oneSigmaBandColour = 1011
    self.twoSigmaBandColour = 1012
    self.signalLineColours = [1013,1013]
#    self.transparentColor1 = ROOT.gROOT.GetColor(1013)#GetColorTransparent(1013,0.5)
#    self.transparentColor1.SetAlpha(0.5)
#    self.signalErrorColours = [self.transparentColor1,self.transparentColor1]
    self.signalErrorColours = [1013,1013]

    # Stat plots
    self.statisticalTestFillColour = 1011
    self.statisticalTestArrowColour = 1013
    self.tomographyGraphColour = 1013

    self.defaultGoodColours = [1010,1011,1012,1013,1014]
    self.shortGoodColours = [1012,1011,1013]

  def setTropicalColours(self) :

    self.blue1 = ROOT.TColor(1000,13.0/255.0,159.0/255.0,216.0/255.0)
    self.mauve = ROOT.TColor(1001,133.0/255.0,105.0/255,207.0/255.0)
    self.green = ROOT.TColor(1002,124.0/255.0,194.0/255.0,66.0/255.0)
    self.darkred = ROOT.TColor(1003,248.0/255.0,14.0/255.0,39.0/255.0)
    self.mango = ROOT.TColor(1004,238.0/255.0, 206.0/255.0, 0.0/255.0)

    # Plot fill colours
    self.oneFillColour = ROOT.TColor(500,72,145,220)
    self.oneFitColour = ROOT.TColor(501,190,15,52)
    self.statisticalTestFillColour = ROOT.TColor(502,158,206,235)
    self.originalStatMarkerColour = ROOT.TColor(503,0,33,71)

    self.fitLineColor = ROOT.kRed

    # Limit-setting plots
    self.oneSigmaBandColour = 1002
    self.twoSigmaBandColour = 1004
    self.signalLineColours = [1003,1003]

#    self.transparentColor1 = ROOT.gROOT.GetColor(1003)
#    self.transparentColor1.SetAlpha(0.5)
#    self.signalErrorColours = [self.transparentColor1,self.transparentColor1]
    self.signalErrorColours = [1003,1003]

    # Stat plots
    self.statisticalTestFillColour = 1000
    self.statisticalTestArrowColour = 1003
    self.tomographyGraphColour = 1003

    # Colours for lists
    self.shortGoodColours = [1000,1002,1003]
    self.defaultGoodColours = [1001,1000,1002,1003,1004] # Red fourth
#    self.defaultGoodColours = [1001,1000,1002,1004,1003]
    self.mediumGoodColours = [ROOT.kCyan+4,ROOT.kCyan+2,ROOT.kCyan,\
                              ROOT.kBlue,ROOT.kBlue+2,\
                              ROOT.kMagenta+2,ROOT.kMagenta,\
                              ROOT.kRed,ROOT.kRed+2,ROOT.kOrange+10,\
                              ROOT.kOrange,ROOT.kYellow]
    self.longGoodColours = [ROOT.kCyan+4,ROOT.kCyan+3,ROOT.kCyan+2,ROOT.kCyan+1,ROOT.kCyan,\
                     ROOT.kBlue,ROOT.kBlue+1,ROOT.kBlue+2,ROOT.kBlue+3,ROOT.kBlue+4,\
                     ROOT.kMagenta+4,ROOT.kMagenta+3,ROOT.kMagenta+2,ROOT.kMagenta+1,ROOT.kMagenta,\
                     ROOT.kRed,ROOT.kRed+1,ROOT.kRed+2,ROOT.kOrange+9,ROOT.kOrange+10,\
                     ROOT.kOrange+7,ROOT.kOrange,ROOT.kYellow]

  def setExtendedTropicalColours(self) :

    # Base colours
    self.blue = ROOT.TColor(1100,0.051,0.627,0.847)#(13.0/255.0,159.0/255.0,216.0/255.0)
    self.mauve = ROOT.TColor(1101,0.522,0.412,0.812)#(133.0/255.0,105.0/255,207.0/255.0)
    self.green = ROOT.TColor(1102,0.486,0.761,0.259)#(124.0/255.0,194.0/255.0,66.0/255.0)
    self.red = ROOT.TColor(1103,0.973,0.055,0.161)#(248.0/255.0,14.0/255.0,39.0/255.0)
    self.mango = ROOT.TColor(1104,0.933,0.808,0.000)#(238.0/255.0, 206.0/255.0, 0.0/255.0)

    # Extended colours
    self.blue1 = ROOT.TColor(1105,0.373,0.745,0.886)
    self.blue2 = ROOT.TColor(1106,0.208,0.682,0.863)
    self.blue3 = ROOT.TColor(1107,0.016,0.545,0.745)
    self.blue4 = ROOT.TColor(1108,0.012,0.384,0.525)

    self.mauve1 = ROOT.TColor(1109,0.816,0.761,0.957)
    self.mauve2 = ROOT.TColor(1110,0.659,0.569,0.890)
    self.mauve3 = ROOT.TColor(1111,0.404,0.282,0.722)
    self.mauve4 = ROOT.TColor(1112,0.306,0.176,0.643)
 
    self.green1 = ROOT.TColor(1113,0.749,0.929,0.596)
    self.green2 = ROOT.TColor(1114,0.624,0.875,0.416)
    self.green3 = ROOT.TColor(1115,0.373,0.663,0.133)
    self.green4 = ROOT.TColor(1116,0.267,0.525,0.051)
    
    self.red1 = ROOT.TColor(1117,0.980,0.404,0.471)
    self.red2 = ROOT.TColor(1118,0.976,0.231,0.318)
    self.red3 = ROOT.TColor(1119,0.933,0.000,0.110)
    self.red4 = ROOT.TColor(1120,0.718,0.000,0.082)

    self.mango1 = ROOT.TColor(1121,1.000,0.914,0.361)
    self.mango2 = ROOT.TColor(1122,1.000,0.890,0.188)
    self.mango3 = ROOT.TColor(1123,0.741,0.639,0.000)
    self.mango4 = ROOT.TColor(1124,0.576,0.498,0.000)

    self.fitLineColor = ROOT.kRed

    # Limit-setting plots
    self.oneSigmaBandColour = 1102
    self.twoSigmaBandColour = 1104
    self.signalLineColours = [1119,1107]
    self.signalErrorColours = [1117,1105]

    # Stat plots
    self.statisticalTestFillColour = 1106
    self.statisticalTestArrowColour = 1119
    self.tomographyGraphColour = 1119

    # Colours for lists
    self.shortGoodColours = [1101,1102,1103]
    self.defaultGoodColours = [1101,1100,1102,1103,1104] # Red fourth
    self.mediumGoodColours = [1101,1100,1102,\
                              1103,1104,\
                              1111,1107,\
                              1115,1119,1123,\
                              1110,1106]
    self.longGoodColours = [1101,1100,1102,1103,1104,\
                     1111,1107,1115,1119,1123,\
                     1110,1106,1114,1118,1122,\
                     1112,1108,1116,1120,1124,\
                     1109,1105,1113,1117,1121]

