# Kate Pachal, March 2013
#
# PyRoot plotting library for importing to individual
# analyses. Will assist in maintaining consistent style
# and comparable plots.

import sys
import ROOT
import AtlasStyle
import AtlasUtils 
import math
import time
from colourPalette import ColourPalette

class Morisot(object) :

  ## ----------------------------------------------------
  ## Initialisers

  def __init__(self) :

    # Set up style
    AtlasStyle.SetAtlasStyle()
    ROOT.gROOT.ForceStyle()

    self.colourpalette = ColourPalette()
#    self.colourpalette.setColourPalette("Teals")
    self.colourpalette.setColourPalette("Tropical")

    self.shortGoodColours = [1001,1002,1003]
    self.defaultGoodColours = [1001,1002,1003,1004,1000]
    self.mediumGoodColours = [ROOT.kCyan+4,ROOT.kCyan+2,ROOT.kCyan,\
                              ROOT.kBlue,ROOT.kBlue+2,\
                              ROOT.kMagenta+2,ROOT.kMagenta,\
                              ROOT.kRed,ROOT.kRed+2,ROOT.kOrange+10,\
                              ROOT.kOrange,ROOT.kYellow]
    self.longGoodColours = [ROOT.kCyan+4,ROOT.kCyan+3,ROOT.kCyan+2,ROOT.kCyan+1,ROOT.kCyan,\
                     ROOT.kBlue,ROOT.kBlue+1,ROOT.kBlue+2,ROOT.kBlue+3,ROOT.kBlue+4,\
                     ROOT.kMagenta+4,ROOT.kMagenta+3,ROOT.kMagenta+2,ROOT.kMagenta+1,ROOT.kMagenta,\
                     ROOT.kRed,ROOT.kRed+1,ROOT.kRed+2,ROOT.kOrange+9,ROOT.kOrange+10,\
                     ROOT.kOrange+7,ROOT.kOrange,ROOT.kYellow]

    self.myLatex = ROOT.TLatex()
    self.myLatex.SetTextColor(ROOT.kBlack)
    self.myLatex.SetNDC()

    self.whitebox = ROOT.TPaveText()
    self.whitebox.SetFillColor(0)
    self.whitebox.SetFillStyle(1001)
    self.whitebox.SetTextColor(ROOT.kBlack)
    self.whitebox.SetTextFont(42)
    self.whitebox.SetTextAlign(11)
    self.whitebox.SetBorderSize(0)

    self.line = ROOT.TLine()

    self.labeltype = 2 # ATLAS internal

    self.doAxisTeV = False # Use histogram's natural units

  def setColourPalette(self,palette) :
    self.colourpalette.setColourPalette(palette)

  def setLabelType(self,type) :
    self.labeltype = type

  def useDoAxisTeV(self,dotev=True) :
    self.doAxisTeV = dotev

  ## ----------------------------------------------------
  ## User-accessible functions

  def drawBasicDataPlot(self,dataHist,luminosity,CME,xname,yname,legendlines,name,binlow=-1,binhigh=-1,doLogY=False,doLogX=False,doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(doLogX)
    c.SetLogy(doLogY)

    if (binlow==-1 and binhigh==-1) :
      firstBin,lastBin = self.getAxisRangeFromHist(dataHist)
    else :
      firstBin = binlow
      lastBin = binhigh
    dataHist.GetXaxis().SetMoreLogLabels()
    if dataHist.GetBinLowEdge(firstBin) > 0.001 and dataHist.GetBinLowEdge(firstBin) < 1 :
      dataHist.GetXaxis().SetNoExponent(ROOT.kTRUE)

    self.drawDataHist(dataHist,firstBin,lastBin,xname,yname,False,1,True)

    lumiInFb = round(float(luminosity)/float(1000),2)

    legendsize = 0.04*len(legendlines)
    if legendsize > 0 :
      if doLogX :
        leftOfLegend = 0.20
        legendbottom = 0.20
        legendtop = legendbottom + legendsize
        print "legendsize is",legendsize
        legend = self.makeLegend(leftOfLegend,legendbottom,0.50,legendtop)
      else :
        leftOfLegend = 0.55
        rightOfLegend = 0.95
        legendtop = 0.90
        legendbottom = legendtop - legendsize
        legend = self.makeLegend(leftOfLegend,legendbottom,rightOfLegend,legendtop)
      legend.AddEntry(dataHist,legendlines[0],"PL")
      legend.Draw()

    if legendsize > 0 and doLogX :
      self.drawLumiAndCMEVert(0.2,legendtop+0.03,lumiInFb,CME)
#      self.drawCMEAndLumi(0.2,legendtop+0.07,CME,lumiInFb)
      self.drawATLASLabels(0.42,0.88)
    else :
      self.drawATLASLabels(0.2, 0.2)
#      self.drawCMEAndLumi(0.2,0.22,CME,lumiInFb)
      self.drawLumiAndCMEVert(0.22,0.28,lumiInFb,CME)


    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawBasicHistogram(self,hist,xname,yname,name,doLogY=False,doLogX=False,doRectangular=False) :
    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(doLogX)
    c.SetLogy(doLogY)

    hist.SetLineColor(ROOT.kBlue+2)
    hist.SetFillColor(0)
    hist.GetXaxis().SetTitle(xname);
    hist.GetYaxis().SetTitle(yname);

    hist.Draw("HIST")

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawBasicMatrix(self,matrix,xname,yname,name) :
    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,False)
    c.SetLogx(1)
    c.SetLogy(1)

#    matrix.SetLineColor(ROOT.kBlue+2)

    ROOT.gStyle.SetPalette(1)

    matrix.Draw("COL")
    matrix.GetXaxis().SetTitle(xname)
    matrix.GetYaxis().SetTitle(yname)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)


  def drawDataWithFitAsFunction(self,dataHist,function,luminosity,CME,xname,yname,legendlines,name,binlow=-1,binhigh=-1,doLogY=False,doLogX=False,doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(doLogX)
    c.SetLogy(doLogY)

    if (binlow==-1 and binhigh==-1) :
      firstBin,lastBin = self.getAxisRangeFromHist(dataHist)
    else :
      firstBin = binlow
      lastBin = binhigh
    dataHist.GetXaxis().SetMoreLogLabels()
    if dataHist.GetBinLowEdge(firstBin) > 0.001 and dataHist.GetBinLowEdge(firstBin) < 1 :
      dataHist.GetXaxis().SetNoExponent(ROOT.kTRUE)

    self.drawDataHist(dataHist,firstBin,lastBin,xname,yname)
    function.SetLineColor(colorpalette.fitLineColor)
    function.Draw("SAME")

    lumiInFb = round(float(luminosity)/float(1000),2)

    legendsize = 0.04*len(legendlines)
    if legendsize > 0 :
      if doLogX :
        leftOfLegend = 0.20
        legendbottom = 0.20
        legendtop = legendbottom + legendsize
        print "legendsize is",legendsize
        legend = self.makeLegend(leftOfLegend,legendbottom,0.50,legendtop)
      else :
        leftOfLegend = 0.55
        rightOfLegend = 0.95
        legendtop = 0.90
        legendbottom = legendtop - legendsize
        legend = self.makeLegend(leftOfLegend,legendbottom,rightOfLegend,legendtop)
      legend.AddEntry(dataHist,legendlines[0],"PL")
      legend.AddEntry(fitHist,legendlines[1],"PL")
      legend.Draw()

    if legendsize > 0 and doLogX :
      self.drawLumiAndCMEVert(0.2,legendtop+0.03,lumiInFb,CME)
      self.drawATLASLabels(0.42,0.88)
    else :
      self.drawATLASLabels(0.2, 0.2)
      self.drawLumiAndCMEVert(0.22,0.28,lumiInFb,CME)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawDataWithFitAsHistogram(self,dataHist,fitHist,luminosity,CME,xname,yname,legendlines,name,drawError=False,binlow=-1,binhigh=-1,doLogY=False,doLogX=False,drawAsSmoothCurve=False,doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(doLogX)
    c.SetLogy(doLogY)

    if (binlow==-1 and binhigh==-1) :
      firstBin,lastBin = self.getAxisRangeFromHist(dataHist)
    else :
      firstBin = binlow
      lastBin = binhigh
    dataHist.GetXaxis().SetMoreLogLabels()
    if dataHist.GetBinLowEdge(firstBin) > 0.001 and dataHist.GetBinLowEdge(firstBin) < 1 :
      dataHist.GetXaxis().SetNoExponent(ROOT.kTRUE)

    print "last few bin contents are"
    for bin in range (lastBin-5,lastBin) :
      print bin, dataHist.GetBinContent(bin)

    self.drawDataHist(dataHist,firstBin,lastBin,xname,yname,False,1,True)
    temp = self.drawFitHist(fitHist,firstBin,lastBin,"","",True,False,drawError)

    lumiInFb = round(float(luminosity)/float(1000),2)

    legendsize = 0.04*len(legendlines)
    if legendsize > 0 :
      if doLogX :
        leftOfLegend = 0.20
        legendbottom = 0.20
        legendtop = legendbottom + legendsize
        print "legendsize is",legendsize
        legend = self.makeLegend(leftOfLegend,legendbottom,0.50,legendtop)
      else :
        leftOfLegend = 0.55
        rightOfLegend = 0.95
        legendtop = 0.90
        legendbottom = legendtop - legendsize
        legend = self.makeLegend(leftOfLegend,legendbottom,rightOfLegend,legendtop)
      legend.AddEntry(dataHist,legendlines[0],"PL")
      if drawError :
        legend.AddEntry(fitHist,legendlines[1],"LF")
      else :
        legend.AddEntry(fitHist,legendlines[1],"PL")
      legend.Draw()

    if legendsize > 0 and doLogX :
      self.drawLumiAndCMEVert(0.2,legendtop+0.03,lumiInFb,CME)
      self.drawATLASLabels(0.42,0.88)
    else :
      self.drawATLASLabels(0.2, 0.2)
      self.drawLumiAndCMEVert(0.22,0.28,lumiInFb,CME)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawSignificanceHistAlone(self,significance,xname,yname,name,doLogX=False,doErrors=False,doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(doLogX)
    c.SetLogy(0)

    firstBin,lastBin = self.getAxisRangeFromHist(significance)
    self.drawSignificanceHist(significance,firstBin+1,lastBin-1,xname,yname,False,False,doLogX,doErrors)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawStackedHistograms(self,histograms,names,xname,yname,name,xmin,xmax,ymin,ymax,doRectangular=False) :
    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(0)
    c.SetLogy(1)

    legend = self.makeLegend(0.5,0.55,0.95,0.90)

    goodcolours = self.getGoodColours(len(histograms))

    stack = ROOT.THStack("stack","stacked histograms")
    for histogram in histograms :
      index = histograms.index(histogram)
      histogram.SetLineColor(goodcolours[index])
      histogram.SetLineWidth(2)
      histogram.SetFillColor(goodcolours[index])
      legend.AddEntry(histogram,names[index],"F")
      histogram.SetTitle("")
      stack.Add(histogram,"hist")

    stack.Draw()
    stack.GetXaxis().SetRangeUser(xmin,xmax)
    stack.SetMaximum(ymax)
    stack.SetMinimum(ymin)

    stack.Draw()
    legend.Draw()

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)


  def drawManyOverlaidHistograms(self,histograms,names,xname,yname,name,xmin,xmax,ymin,ymax,extraLegendLines = [],doLogX=False,doLogY=True,doErrors=False,doRectangular=False,doLegend=True,doLegendLow=True,doLegendLocation="Left",doLegendOutsidePlot=False,doATLASLabel="Low",pairNeighbouringLines=False,dotLines = [],addHorizontalLines=[]) :
    # Left, Right, or Wide

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular,doLegendOutsidePlot)
    c.SetLogx(doLogX)
    c.SetLogy(doLogY)

    if doLegendOutsidePlot :
      outpad = ROOT.TPad("extpad","extpad",0,0,1,1) # For marking outermost dimensions
      pad1 = ROOT.TPad("pad1","pad1",0,0,0.5,1) # For main histo
      pad2 = ROOT.TPad("pad2","pad2",0.5,0,1,1) # For signal significance histo

      # Set up to draw in right orientations
      outpad.SetFillStyle(4000) #transparent
      pad1.SetBorderMode(0)
      pad1.SetLogy(doLogY)
      pad1.SetLogx(doLogX)
      pad2.SetBorderMode(0)
      pad1.Draw()
      pad2.Draw()
      outpad.Draw()

      pad1.cd()

    lowxvals = []
    lowyvals = []
    lownonzeros = []
    highxvals = []
    highyvals = []
    for histogram in histograms :
      lowx,highx = self.getAxisRangeFromHist(histogram)
      lowy,lownonzero,highy = self.getYRangeFromHist(histogram)
      lowxvals.append(lowx)
      highxvals.append(highx)
      lownonzeros.append(lownonzero)
      lowyvals.append(lowy)
      highyvals.append(highy)
    lowxvals.sort()
    lowyvals.sort()
    lownonzeros.sort()
    highxvals.sort()
    highyvals.sort()
    if xmin == 'automatic':
      minX = lowxvals[0]
    else :
      minX = xmin
    if xmax == 'automatic':
      maxX = highxvals[-1]
    else :
      maxX = xmax
    if ymin == 'automatic':
      if doLogY :
        minY = lownonzeros[0]/2.0
      else :
        minY = lowyvals[0]
    else :
      minY = ymin
    if ymax == 'automatic':
      if doLogY :
        maxY = highyvals[-1]*100
      else :
        maxY = highyvals[-1]*1.5
    else :
      maxY = ymax

    # Create legend
    legendsize = 0.04*len(histograms)
    if doLegendOutsidePlot :
      pad2.cd()
      leftOfLegend = 0
      legendbottom = 0.2
      legendtop = 0.95
      legend = self.makeLegend(leftOfLegend,legendbottom,1.0,legendtop)
      pad1.cd()
    else :
      if doLegendLow :
        legendbottom = 0.20
        legendtop = legendbottom + legendsize
      else :
        legendtop = 0.90 - (0.05)*len(extraLegendLines)
        if doATLASLabel != "Low" and doATLASLabel != "None":
          legendtop = legendtop - 0.05
        legendbottom = legendtop - legendsize
      if doLegendLocation == "Left" :
        leftOfLegend = 0.20
        rightOfLegend = 0.60
      elif doLegendLocation == "Right" :
        leftOfLegend = 0.54
        rightOfLegend = 0.95
      elif doLegendLocation == "Wide" :
        leftOfLegend = 0.20
        rightOfLegend = 0.95
      legend = self.makeLegend(leftOfLegend,legendbottom,rightOfLegend,legendtop)
      if doLegendLocation == "Wide" and len(histograms) > 5 :
        legend.SetNColumns(2)

    if pairNeighbouringLines :
      goodcolours = self.getGoodColours(len(histograms)/2+1)
    else :
      goodcolours = self.getGoodColours(len(histograms))

    for histogram in histograms :
      index = histograms.index(histogram)
      if pairNeighbouringLines :
        histogram.SetLineColor(goodcolours[int(index/2.0)])
        histogram.SetMarkerColor(goodcolours[int(index/2.0)])
        if index % 2 == 1 :
          histogram.SetLineStyle(2)
      else :
        histogram.SetLineColor(goodcolours[index])
        histogram.SetMarkerColor(goodcolours[index])
        histogram.SetLineStyle(1)
        if dotLines != [] and dotLines[index] == True:
          histogram.SetLineStyle(index+1)

      histogram.SetLineWidth(2)
      histogram.SetFillStyle(0)
      histogram.SetTitle("")
      histogram.GetXaxis().SetRange(minX,maxX+5)
      histogram.GetYaxis().SetRangeUser(minY,maxY)
      histogram.GetXaxis().SetNdivisions(605,ROOT.kTRUE)
      legend.AddEntry(histogram,names[index],"PL")
      if (index==0) :
        histogram.GetXaxis().SetTitle(xname)
        histogram.GetYaxis().SetTitle(yname)
        if doLogX :
          histogram.GetXaxis().SetMoreLogLabels()
        if not doErrors :
          histogram.Draw("HIST")
        else :
          histogram.Draw("E")
      else :
        histogram.GetXaxis().SetTitle("")
        histogram.GetYaxis().SetTitle("")
        if not doErrors :
          histogram.Draw("HIST SAME")
        else :
          histogram.Draw("E SAME")

    if (doLegend) :

      if doLegendOutsidePlot :
        index = 0
        if len(extraLegendLines) > 0 :
#          if doLegendLow :
#            leftOfLegend = 0.15
#            legendtop = 0.20
#          else :
#            if doLegendLeft :
#              leftOfLegend = 0.20
#            else :
#              leftOfLegend = 0.55
#            legendtop = 0.90 - (0.06)*len(extraLegendLines)
          for item in range(len(extraLegendLines)) :
            toplocation = legendtop +0.02 + (0.01+0.05)*(index)
            item = self.myLatex.DrawLatex(leftOfLegend+0.02,toplocation,extraLegendLines[index])
            index = index+1
        pad2.cd()
        if len(histograms) > 15 :
          legend.SetNColumns(2)
        legend.Draw()
        pad1.cd()

      else :
        index = 0
        if len(extraLegendLines) > 0 :
          for line in extraLegendLines :
            toplocation = legendtop +0.02 + (0.01+0.04)*(index)
            if doLegendLocation == "Left" :
              extralength = 0 
            else :
              extralength = float(max(len(line) - 19,0)/35.0)
            item = self.myLatex.DrawLatex(leftOfLegend+0.01-extralength,toplocation,line)
            index = index+1
        legend.Draw()

    if doATLASLabel == "Low" :
      if doLegendLow :
        self.drawATLASLabels(0.22,0.88,False,doRectangular)
      else :
        self.drawATLASLabels(0.2, 0.2,False,doRectangular)
    elif doATLASLabel == "None" :
      pass
    else :
      if doLegendLocation == "Left" :
        self.drawATLASLabels(0.2,0.88)
      else :
        self.drawATLASLabels(0.53,0.88, True)

    if addHorizontalLines != [] :
      for val in addHorizontalLines :
        line = ROOT.TLine(histograms[0].GetBinLowEdge(minX), val, histograms[0].GetBinLowEdge(maxX+6), val)
        line.SetLineColor(ROOT.kBlack)
        line.SetLineStyle(2)
        line.Draw("SAME")

    if doLegendOutsidePlot :
      pad1.RedrawAxis()
    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawTwoHistsDifferentYAxes(self,hist1,hist2,xname,yname1,yname2,name,doRectangular=False) :
    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(0)
    c.SetLogy(1)

    firstBin1,lastBin1 = self.getAxisRangeFromHist(hist1)
    firstBin2,lastBin2 = self.getAxisRangeFromHist(hist2)
    firstBin = min(firstBin1,firstBin2)
    lastBin = max(lastBin1,lastBin2)

    hists = [hist1,hist2]

    legend = self.makeLegend(0.5,0.7,0.95,0.90)
    goodcolours = self.getGoodColours(2)

    topval = 0
    for histogram in hists :
      index = hists.index(histogram)
      histogram.SetLineColor(goodcolours[index])
      histogram.SetLineWidth(2)
      histogram.SetFillStyle(0)
      histogram.SetTitle("")
      legend.AddEntry(histogram,histogram.GetName(),"PL")
      if (index==0) :
        firstbin,lastbin = self.getAxisRangeFromHist(histogram)
        firsttouse = firstbin-5
        if firsttouse<1 :
          firsttouse = 1
        lasttouse = lastbin+5
        if lasttouse > histogram.GetNbinsX() :
          lasttouse = histogram.GetNbinsX()
        histogram.GetXaxis().SetTitle(xname)
        histogram.GetXaxis().SetRange(firsttouse,lasttouse)
        histogram.GetYaxis().SetTitle(yname1)
        histogram.Draw("HIST")
        firstbin,lastbin
        topval = histogram.GetMaximum()

      else :
        scale = topval/histogram.GetMaximum()
        histogram.Scale(scale)
        histogram.Draw("HIST SAME")
#        print ROOT.gPad.GetUxmax(),ROOT.gPad.GetUymin(),ROOT.gPad.GetUxmax(),ROOT.gPad.GetUymax(),0.0,1.1
#        axis = ROOT.TGaxis(1.0,0.0,1.0,1.0,0.0,1.1,510,"+L")
#        axis.SetLineColor(goodcolours[index])
#        axis.SetTextColor(goodcolours[index])
#        axis.Draw()

      legend.Draw()

    c.Update()
    c.SaveAs(outputname)
 

  def drawPseudoExperimentsWithObservedStat(self,pseudoStatHist,observedStat,luminosity,CME,xname,yname,name,doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(0)
    c.SetLogy(1)

    # Draw stats from pseudoexperiments
    pseudoStatHist.SetLineColor(ROOT.kBlack)
    pseudoStatHist.SetFillColor(self.colourpalette.statisticalTestFillColour) # ROOT.kYellow
    pseudoStatHist.SetFillStyle(1001)
    pseudoStatHist.Draw("HIST")
    pseudoStatHist.GetYaxis().SetRangeUser(0.5,(pseudoStatHist.GetBinContent(pseudoStatHist.GetMaximumBin()))*50)
    pseudoStatHist.GetYaxis().SetTitleOffset(1.4)
    pseudoStatHist.GetXaxis().SetTitle(xname)
    pseudoStatHist.GetYaxis().SetTitle(yname)

    # Draw arrow to observed stat
    arrow = ROOT.TArrow()
    arrow.SetLineColor(self.colourpalette.statisticalTestArrowColour)
    arrow.SetFillColor(self.colourpalette.statisticalTestArrowColour)
    arrow.SetLineWidth(2)
    arrow.DrawArrow(observedStat,1,observedStat,0)

    # Create legend
    legend = self.makeLegend(0.21,0.68,0.75,0.78)
    lumInFb = round(float(luminosity)/float(1000),2)
    legend.AddEntry(pseudoStatHist,"Pseudo-experiments","LF")
    legend.AddEntry(arrow,"Value in Data","L")
    legend.Draw()

    lumInFb = round(float(luminosity)/float(1000),2)
    self.drawATLASLabels(0.22,0.88)
    self.drawCMEAndLumi(0.22,0.82,CME,lumInFb,0.04)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawBumpHunterTomographyPlot(self,tomographyGraph,name) :
    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,False)
    c.SetLogx(1)
    c.SetLogy(1)
    c.SetGridx(0)
    c.SetGridy(0)

    tomographyGraph.SetLineColor(self.colourpalette.tomographyGraphColour)
    tomographyGraph.SetTitle("");
    tomographyGraph.GetXaxis().SetTitle("Dijet Mass [GeV]")
    tomographyGraph.GetXaxis().SetNdivisions(805,ROOT.kTRUE)
    tomographyGraph.GetYaxis().SetTitle("Poisson PVal of Interval")
    tomographyGraph.GetYaxis().SetTitleOffset(1.2);

    tomographyGraph.SetMarkerColor(self.colourpalette.tomographyGraphColour)
    tomographyGraph.SetMarkerSize(0.2)

    tomographyGraph.Draw("AP");

    self.drawATLASLabels(0.60, 0.20)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawDataAndFitOverSignificanceHist(self,dataHist,fitHist,significance,x,datay,sigy,name,luminosity,CME,firstBin=-1,lastBin=-1,doBumpLimits=False,bumpLow=0,bumpHigh=0,extraLegendLines=[],doLogX=True,doRectangular=False,setYRange=[]) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,False)
    c.SetLogx(1)
    c.SetLogy(doLogX)
    c.SetGridx(0)
    c.SetGridy(0)

    # Dimensions: xlow, ylow, xup, yup
    outpad = ROOT.TPad("extpad","extpad",0,0,1,1) # For marking outermost dimensions
    pad1 = ROOT.TPad("pad1","pad1",0,0.33,1,1) # For main histo
    pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.33) # For residuals histo

    # Set up to draw in right orientations
    outpad.SetFillStyle(4000) #transparent
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad1.SetLogy(1)
    pad1.SetLogx(doLogX)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.3)
    pad2.SetBorderMode(0)
    pad2.SetLogx(doLogX)
    pad1.Draw()
    pad2.Draw()
    outpad.Draw()

    # Draw data and fit histograms
    pad1.cd()

    # Use bin range within which bkgPlot has entries, 
    # plus one empty on either side if available
    print "firstbin, lastbin are",firstBin,lastBin
    lowbin,highbin = self.getAxisRangeFromHist(dataHist)
    if (firstBin>0) :
      lowbin=firstBin
    if (lastBin>0 and lastBin>=firstBin) :
      highbin = lastBin

    fitHist.GetYaxis().SetTitleSize(0.05)
    fitHist.GetYaxis().SetTitleOffset(1.0)
    fitHist.GetYaxis().SetLabelSize(0.05)
    self.drawFitHist(fitHist,lowbin,highbin,x,datay,False,True)
    self.drawDataHist(dataHist,lowbin,highbin,"","",True,2)
    firstBinWithData,lastBinWithData = self.getAxisRangeFromHist(dataHist)
    if setYRange == [] :
      fitHist.GetYaxis().SetRangeUser(0.5,dataHist.GetBinContent(firstBinWithData+1)*10)
      dataHist.GetYaxis().SetRangeUser(0.5,dataHist.GetBinContent(firstBinWithData+1)*10)
    else :
      fitHist.GetYaxis().SetRangeUser(setYRange[0],setYRange[1])
      dataHist.GetYaxis().SetRangeUser(setYRange[0],setYRange[1])

    # Draw significance histogram
    pad2.cd()
    significance.GetYaxis().SetTitleSize(0.1)
    significance.GetYaxis().SetTitleOffset(0.42) # 1.2 = 20% larger
    significance.GetYaxis().SetLabelSize(0.1)
    significance.GetXaxis().SetLabelSize(0.1)
    significance.GetXaxis().SetTitleSize(0.1)
    significance.GetXaxis().SetTitleOffset(1.2)
    if (doLogX) :
      significance.GetXaxis().SetMoreLogLabels()
    self.drawSignificanceHist(significance,firstBin,lastBin,x,sigy,True)
    c.Update()

    # in place of ROOT.TLine()
    line1 = self.line.Clone("line1"); line1lims = [] 
    line2 = self.line.Clone("line2"); line2lims = []
    line3 = self.line.Clone("line3"); line3lims = []
    line4 = self.line.Clone("line4"); line4lims = []
    if doBumpLimits :
      heightLowEdge=0
      heightHighEdge=0
      minYvalue = dataHist.GetMinimum()
      for i in range(dataHist.GetNbinsX()) :
        locationOfTallEdge = dataHist.GetBinLowEdge(i)
        height = dataHist.GetBinContent(i)
        if locationOfTallEdge == bumpLow :
          heightLowEdge = height
        if locationOfTallEdge == bumpHigh:
          heightHighEdge = height

      lowYVal = significance.GetMinimum()#-0.2
      highYVal = significance.GetMaximum()#+0.2

      line1lims = [bumpLow,minYvalue,bumpLow,heightLowEdge]
      line2lims = [bumpHigh,minYvalue,bumpHigh,heightHighEdge]
      line3lims = [bumpLow,lowYVal,bumpLow,highYVal]
      line4lims = [bumpHigh,lowYVal,bumpHigh,highYVal]

#    if doBumpLimits :
      print "in 2"
      # Draw blue lines
      pad1.cd()
      line1.SetLineColor(ROOT.kBlue)
      line1.SetX1(line1lims[0]); line1.SetY1(line1lims[1]); line1.SetX2(line1lims[2]); line1.SetY2(line1lims[3])
      line1.Draw()
      line2.SetLineColor(ROOT.kBlue)
      line2.SetX1(line2lims[0]); line2.SetY1(line2lims[1]); line2.SetX2(line2lims[2]); line2.SetY2(line2lims[3])
      line2.Draw()
      pad2.cd()
      line3.SetLineColor(ROOT.kBlue)
      line3.SetX1(line3lims[0]); line3.SetY1(line3lims[1]); line3.SetX2(line3lims[2]); line3.SetY2(line3lims[3])
      line3.Draw()
      line4.SetLineColor(ROOT.kBlue)
      line4.SetX1(line4lims[0]); line4.SetY1(line4lims[1]); line4.SetX2(line4lims[2]); line4.SetY2(line4lims[3])
      line4.Draw()

    c.Update()

    outpad.cd()
    leftOfLegend = 0.60
    widthOfRow = 0.04
    lumInFb = round(float(luminosity)/float(1000),2)
    if (doLogX and not doBumpLimits) :
      self.drawATLASLabels(0.2, 0.35)
      self.drawCMEAndLumi(0.52,0.90,CME,lumInFb,0.04)
      bottomOfLegend = 0.78
      legend = self.makeLegend(leftOfLegend,bottomOfLegend,0.9,0.87)
    else :
      self.drawATLASLabels(0.53, 0.9, True)
      self.drawCMEAndLumi(0.51,0.84,CME,lumInFb,0.04)
      bottomOfLegend = 0.72
      legend = self.makeLegend(leftOfLegend,bottomOfLegend,0.9,0.81)

    c.Update()

    self.myLatex.SetTextFont(42)
    self.myLatex.SetTextSize(0.04)
    index = 0
    persistent = []
    if len(extraLegendLines) > 0 :
#      toplocation = bottomOfLegend - (0.01+widthOfRow)*(index) #topOfAll - (0.03+2*widthOfRow) - (0.01+widthOfRow)*(index)
      for line in extraLegendLines :
        toplocation = bottomOfLegend - (0.01+widthOfRow)*(index) #topOfAll - (0.03+2*widthOfRow) - (0.01+widthOfRow)*(index)
        persistent.append(self.myLatex.DrawLatex(leftOfLegend+0.01,toplocation,line))
        index = index+1

    # Go to outer pad to fill and draw legend
    # Create legend
    outpad.cd()
    legend.AddEntry(dataHist,"Data","LFP")
    legend.AddEntry(fitHist,"Background","LF")
    legend.Draw()
    c.Update()

    # Save.
    pad1.RedrawAxis()
    pad2.RedrawAxis()
    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawHistsOverSignificanceHists(self,histograms,names,significancehists,xname,yname,sigy,name,luminosity,CME,xmin,xmax,ymin,ymax,doLogX=True,doLogY=True,doRectangular=False,doErrMain=False,doErrSig=False,sigHistRange=[]) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,False)
    c.SetLogx(1)
    c.SetLogy(doLogX)
    c.SetGridx(0)
    c.SetGridy(0)

    # Dimensions: xlow, ylow, xup, yup
    outpad = ROOT.TPad("extpad","extpad",0,0,1,1) # For marking outermost dimensions
    pad1 = ROOT.TPad("pad1","pad1",0,0.33,1,1) # For main histo
    pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.33) # For residuals histo

    # Set up to draw in right orientations
    outpad.SetFillStyle(4000) #transparent
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad1.SetLogy(1)
    pad1.SetLogx(doLogX)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.3)
    pad2.SetBorderMode(0)
    pad2.SetLogy(0)
    pad2.SetLogx(doLogX)
    pad1.Draw()
    pad2.Draw()
    outpad.Draw()

    # Draw data and fit histograms
    pad1.cd()

    # Use bin range within which are all plot entries, 
    # plus one empty on either side if available
    minmaxes = []
    index=-1
    for histlist in [histograms,significancehists] :
      index = index+1
      lowxvals = []
      lowyvals = []
      lownonzeros = []
      highxvals = []
      highyvals = []
      for histogram in histlist :
        lowx,highx = self.getAxisRangeFromHist(histogram)
        lowy,lownonzero,highy = self.getYRangeFromHist(histogram)
        lowxvals.append(lowx)
        highxvals.append(highx)
        lownonzeros.append(lownonzero)
        lowyvals.append(lowy)
        highyvals.append(highy)
      lowxvals.sort()
      lowyvals.sort()
      lownonzeros.sort()
      highxvals.sort()
      highyvals.sort()
      if xmin == 'automatic':
        minX = lowxvals[0]
      else :
        minX = xmin
      if xmax == 'automatic':
        maxX = highxvals[-1]
      else :
        maxX = xmax
      if ymin == 'automatic':
        if doLogY and index==0 :
          minY = lownonzeros[0]/2.0
        else:
          minY = lowyvals[0]
      else :
        minY = ymin
      if ymax == 'automatic':
        if doLogY and index==0:
          maxY = highyvals[-1]*100
        else :
          maxY = highyvals[-1]*1.5
      else :
        maxY = ymax
      minmaxes.append([minX,maxX,minY,maxY])

    goodcolours = self.getGoodColours(len(histograms))

    range = minmaxes[0]
    minX = range[0]; maxX = range[1]; minY = range[2]; maxY = range[3]
    for histogram in histograms :
      index = histograms.index(histogram)
      histogram.SetLineColor(goodcolours[index])
      histogram.SetMarkerColor(goodcolours[index])
      histogram.SetLineStyle(1)
      histogram.SetLineWidth(2)
      histogram.SetFillStyle(0)
      histogram.SetTitle("")
      histogram.GetXaxis().SetRange(minX,maxX+5)
      histogram.GetYaxis().SetRangeUser(minY,maxY)
      histogram.GetXaxis().SetNdivisions(605,ROOT.kTRUE)
      if (index==0) :
        histogram.GetYaxis().SetTitleSize(0.05)
        histogram.GetYaxis().SetTitleOffset(1.2) #1.0
        histogram.GetYaxis().SetLabelSize(0.05)

        histogram.GetXaxis().SetTitle(xname)
        histogram.GetYaxis().SetTitle(yname)
        if doErrMain :
          histogram.Draw("E")
        else :
          histogram.Draw("HIST")
      else :
        histogram.GetXaxis().SetTitle("")
        histogram.GetYaxis().SetTitle("")
        if doErrMain :
          histogram.Draw("E SAME")
        else :
          histogram.Draw("HIST SAME")

    # Draw significance histograms
    pad2.cd()
    range = minmaxes[1]
    minY = range[2]; maxY = range[3]
    if sigHistRange != [] :
      minY = sigHistRange[0]
      maxY = sigHistRange[1]
    # find nearest 0.25 to maxY
    for histogram in significancehists :
      index = significancehists.index(histogram)
      histogram.SetLineColor(goodcolours[index])
      histogram.SetMarkerColor(goodcolours[index])
      histogram.SetLineStyle(1)
      histogram.SetLineWidth(2)
      histogram.SetFillStyle(0)
      histogram.SetTitle("")
      histogram.GetXaxis().SetRange(minX,maxX+5)
      histogram.GetYaxis().SetRangeUser(minY,maxY)
      histogram.GetXaxis().SetNdivisions(605,ROOT.kTRUE)
      if (index==0) :
        histogram.GetYaxis().SetTitleSize(0.1)
        histogram.GetYaxis().SetTitleOffset(0.6) #0.42 # 1.2 = 20% larger
        histogram.GetYaxis().SetLabelSize(0.1)
        histogram.GetXaxis().SetLabelSize(0.1)
        histogram.GetXaxis().SetTitleSize(0.1)
        histogram.GetXaxis().SetTitleOffset(1.2)
        histogram.GetXaxis().SetNdivisions(805,ROOT.kTRUE)
        if doLogX :
          histogram.GetXaxis().SetMoreLogLabels()
        histogram.GetXaxis().SetTitle(xname)
        histogram.GetYaxis().SetTitle(sigy)
        if doErrSig :
          histogram.Draw("E")
        else :
          histogram.Draw("HIST")
      else :
        histogram.GetXaxis().SetTitle("")
        histogram.GetYaxis().SetTitle("")
        if doErrSig :
          histogram.Draw("E SAME")
        else :
          histogram.Draw("HIST SAME")

    outpad.cd()
    persistent = []
    lumInFb = round(float(luminosity)/float(1000),2)

    lshift = 0
    maxlen = 0
    for name in names :
      if len(name) > 12 and len(name) > maxlen :
        lshift = 0.0 - 0.01*(len(name)-12)
        maxlen = len(name)

    if (doLogX) :
      legend = self.makeLegend(0.60+lshift,0.75,0.9,0.87)
    else :
      legend = self.makeLegend(0.60+lshift,0.71,0.9,0.82)
    for histogram in histograms :
      legend.AddEntry(histogram,names[histograms.index(histogram)],"PL")
    if (doLogX) :
      self.drawATLASLabels(0.2, 0.35)
      self.drawCMEAndLumi(0.52,0.90,CME,lumInFb,0.04)
    else :
      self.drawATLASLabels(0.53, 0.9, True)
      self.drawCMEAndLumi(0.52,0.83,CME,lumInFb,0.04)

    # Go to outer pad to fill and draw legend
    # Create legend
    legend.Draw()

    # Save.
    pad1.RedrawAxis()
    pad2.RedrawAxis()
    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)


  def drawDataAndFitWithSignalsOverSignificances(self,dataHist,fitHist,signalsignificance,residual,signalsForSpec,signalsForSig,signalmasses,legendlist,x,datay,sigy,residy,name,luminosity,CME,firstBin=-1,lastBin=-1,doBumpLimits=False,bumpLow=0,bumpHigh=0,doLogX=True,doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,False)
    c.SetLogx(1)
    c.SetGridx(0)
    c.SetGridy(0)

    # Dimensions: xlow, ylow, xup, yup
    outpad = ROOT.TPad("extpad","extpad",0,0,1,1) # For marking outermost dimensions
    pad1 = ROOT.TPad("pad1","pad1",0,0.36,1,1) # For main histo
    pad2 = ROOT.TPad("pad2","pad2",0,0.23,1,0.36) # For signal significance histo
    pad3 = ROOT.TPad("pad3","pad3",0,0,1,0.23) # For residuals histo

    # Set up to draw in right orientations
    outpad.SetFillStyle(4000) #transparent
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad1.SetLogy(1)
    pad1.SetLogx(doLogX)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.00001)
    pad2.SetBorderMode(0)
    pad2.SetLogx(doLogX)
    pad3.SetTopMargin(0.00001)
    pad3.SetBottomMargin(0.43)
    pad3.SetBorderMode(0)
    pad3.SetLogx(doLogX)
    pad1.Draw()
    pad2.Draw()
    pad3.Draw()
    outpad.Draw()

    # Use bin range within which bkgPlot has entries, 
    # plus one empty on either side if available
    lowbin,highbin = self.getAxisRangeFromHist(dataHist)
    if (firstBin>0) :
      lowbin=firstBin
    if (lastBin>0 and lastBin>=firstBin) :
      highbin = lastBin

    # Draw data and fit histograms
    pad1.cd()
    fitHist.GetYaxis().SetTitleSize(0.06)
    fitHist.GetYaxis().SetTitleOffset(0.8)
    fitHist.GetYaxis().SetLabelSize(0.05)
    self.drawSignalOverlaidOnDataAndFit(dataHist,fitHist,signalsForSpec,signalmasses,[],luminosity,CME,datay,"",firstBin,lastBin,doLogX,True,False,3)
    pad1.Update()

    # Draw signals significance histogram
    pad2.cd()
    signalsignificance.GetYaxis().SetTitleSize(0.21)
    signalsignificance.GetYaxis().SetTitleOffset(0.18) # 1.2 = 20% larger
    signalsignificance.GetYaxis().SetLabelSize(0.2)
    self.drawSignalOverlaidOnBkgPlot(signalsignificance,signalsForSig,signalmasses,[],luminosity,CME,sigy,"",firstBin,lastBin,False,False)
    pad2.Update()

    # Draw residual histogram
    pad3.cd()
    residual.GetYaxis().SetTitleSize(0.12)
    residual.GetYaxis().SetTitleOffset(0.32) # 1.2 = 20% larger
    residual.GetYaxis().SetLabelSize(0.115)
    residual.GetXaxis().SetLabelSize(0.15)
    residual.GetXaxis().SetTitleSize(0.17)
    residual.GetXaxis().SetTitleOffset(1.2)
    residual.GetXaxis().SetMoreLogLabels()
    if residual.GetBinLowEdge(firstBin) > 0.001 and residual.GetBinLowEdge(firstBin) < 1 :
      residual.GetXaxis().SetNoExponent(ROOT.kTRUE)
    self.drawSignificanceHist(residual,firstBin,lastBin,x,residy,True,True)
    pad3.Update()

    # Go to outer pad to fill and draw legend
    # Create legend
    outpad.cd()

    lumInFb = round(float(luminosity)/float(1000),2)
    if (doLogX) :
      if self.labeltype == 1 or self.labeltype == 3 :
        self.drawATLASLabels(0.53, 0.9)
      elif self.labeltype == 0 :
        self.drawATLASLabels(0.75, 0.9)
      else :
        self.drawATLASLabels(0.6, 0.9)
      self.drawCMEAndLumi(0.53,0.84,CME,lumInFb,0.04)
      topOfLegend = 0.65
      leftOfLegend = 0.2
    else :
      self.drawATLASLabels(0.2, 0.4)
      topOfLegend = 0.87
      leftOfLegend = 0.5
      self.drawCMEAndLumi(leftOfLegend,topOfLegend+0.02,CME,lumInFb,0.04)
    widthOfRow = 0.05
    bottomOfLegend = topOfLegend-(widthOfRow*(len(self.saveplots)+2))
    rightOfLegend = leftOfLegend+0.4
    legend = self.makeLegend(leftOfLegend,bottomOfLegend,rightOfLegend,topOfLegend)

    legend.AddEntry(dataHist,"Data","LFP")
    legend.AddEntry(fitHist,"Fit","LF")
    for plot in self.saveplots :
      index = self.saveplots.index(plot)
      legend.AddEntry(plot,legendlist[index],"LP")
    legend.Draw()

    # Save.
    c.Update()
#    pad1.RedrawAxis("g")
#    c.RedrawAxis("g")
    ROOT.gPad.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)


  def drawLimitSettingPlot2Sigma(self,observed,expected1sigma,expected2sigma,signals,signalslegend,name,nameX,nameY,luminosity,CME,xmin,xmax,ymin,ymax,doRectangular=False) :

    if type(signals) is not list :
      signals = [signals]
      signalslegend = [signalslegend]

    canvasname = name+'_cv'
    outputname = name+'.eps'
    editableoutputname = name+'.C'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(0)
    c.SetLogy(1)
#    c.SetLogy(0)
    c.SetGridx(0)
    c.SetGridy(0)

    # Set automatic axis range from graphs.
    # X axis range will be exactly ends of graphs
    xVals = []
    for i in range(observed.GetN()) :
      xVals.append(observed.GetX()[i])
    xVals.sort()
    if xmin == 'automatic':
      minX = xVals[-1]
    else :
      minX = xmin
    if xmax == 'automatic':
      maxX = xVals[0]
    else :
      maxX = xmax
    # Y axis range will be 3 orders of magnitude
    # above highest point of observed, because want space for legend
    # Lowest point should be 2 orders of magnitude below lowest point in observed
    if ymin == 'automatic' :
      minY = observed.GetMinimum()/100
    else :
      minY = ymin
    if ymax == 'automatic' :
      maxY = observed.GetMaximum()*1000
    else :
      maxY = ymax

    # Set axis names.
    # So far, should always be thus so don't pass as parameters.

    # Create legend
    leftOfLegend = 0.28
    bottomOfLegend = 0.68-0.05*(len(signals)-1) # 0.64
    legend = self.makeLegend(leftOfLegend,bottomOfLegend,0.95,0.92) # 0.88

    # Set up display for expectations
    for graph,colour in [[expected2sigma,self.colourpalette.twoSigmaBandColour],[expected1sigma,self.colourpalette.oneSigmaBandColour]] :
      graph.SetMarkerColor(1)
      graph.SetMarkerSize(1)
      graph.SetMarkerStyle(20)
      graph.SetLineColor(1)
      graph.SetLineWidth(3)
      graph.SetLineStyle(3)
      graph.SetFillColor(colour)
      graph.GetXaxis().SetTitle(nameX)
      graph.GetYaxis().SetTitle(nameY)
      graph.GetXaxis().SetRangeUser(minX,maxX)
      graph.GetXaxis().SetNdivisions(705,ROOT.kTRUE)
      graph.GetYaxis().SetRangeUser(minY,maxY)
      if minX > 0.001 and minX < 1 :
        graph.GetXaxis().SetNoExponent(ROOT.kTRUE)

    # Set up display for signal
    for signal in signals :
      thiscolour = self.colourpalette.signalLineColours[signals.index(signal)]
      thiserrorcolour = self.colourpalette.signalErrorColours[signals.index(signal)]
      signal.SetMarkerColor(4)
      signal.SetMarkerSize(1)
      signal.SetMarkerStyle(24)
      signal.SetLineColor(thiscolour)
      signal.SetLineWidth(2)
      signal.SetLineStyle(9 - signals.index(signal))
      signal.SetFillColor(thiserrorcolour)
#      signal.SetFillStyle(3006)
      signal.GetXaxis().SetTitle(nameX)
      signal.GetXaxis().SetNdivisions(705,ROOT.kTRUE)
      signal.GetYaxis().SetTitle(nameY)
      signal.GetXaxis().SetRangeUser(minX,maxX)
      signal.GetYaxis().SetRangeUser(minY,maxY)
      if minX > 0.001 and minX < 1 :
        signal.GetXaxis().SetNoExponent(ROOT.kTRUE)

    # Set up display for observations
    observed.SetMarkerColor(1)
    observed.SetMarkerSize(1)
    observed.SetMarkerStyle(20)
    observed.SetLineColor(1)
    observed.SetLineWidth(3)
    observed.SetLineStyle(1)
    observed.SetFillColor(0)
    observed.GetXaxis().SetTitle(nameX)
    observed.GetYaxis().SetTitle(nameY)
    observed.GetXaxis().SetRangeUser(minX,maxX)
    observed.GetYaxis().SetRangeUser(minY,maxY)
    if minX > 0.001 and minX < 1 :
      observed.GetXaxis().SetNoExponent(ROOT.kTRUE)

    # First one has to include axes or everything comes out blank
    # Rest have to NOT include axes or each successive one overwrites
    # previous. "SAME option does not exist for TGraph classes.
    expected2sigma.Draw("A3") # 2-sigma expectation error bands
    expected1sigma.Draw("L3") # 1-sigma expectation error bands
    expected1sigma.Draw("LX") # Center of expectation
    for signal in signals :
      signal.Draw("03") #L03
    for signal in signals :
      signal.Draw("LX") # was CX
    observed.Draw("PL") # Data points of measurement

    # Fill and draw legend
    for signal in signals :
      index = signals.index(signal)
      legend.AddEntry(signal,signalslegend[index],"LF")#"L")
    legend.AddEntry(observed,"Observed 95% CL upper limit","PL")
    legend.AddEntry(expected1sigma, "Expected 95% CL upper limit","L")
    legend.AddEntry( "NULL" , "68% and 95% bands","")

    self.drawATLASLabels(0.20,0.20)
    legend.Draw()

    shadeBox = ROOT.TBox()
    boxX1 = minX + (maxX - minX)*0.19 #21
    boxX2 = boxX1 + (maxX - minX)*0.125 # 135
    if c.GetLogy() :
      boxY1 = math.exp(math.log(minY) + (math.log(maxY) - math.log(minY))*(0.665-0.06*(len(signals)-1))) #0.66
      boxY2 = math.exp(math.log(boxY1) + (math.log(maxY) - math.log(minY))*0.0155)
      boxY3 = math.exp(math.log(boxY2) + (math.log(maxY) - math.log(minY))*0.025)
      boxY4 = math.exp(math.log(boxY3) + (math.log(maxY) - math.log(minY))*0.0155)
    else :
      boxY1 = minY + (maxY - minY)*(0.665-0.06*(len(signals)-1)) #0.66
      boxY2 = boxY1 + (maxY - minY)*0.0155
      boxY3 = boxY2 + (maxY - minY)*0.025
      boxY4 = boxY3 + (maxY - minY)*0.0155

    shadeBox.SetFillColor(self.colourpalette.twoSigmaBandColour)
    shadeBox.DrawBox(boxX1,boxY1,boxX2,boxY4)
    shadeBox.SetFillColor(self.colourpalette.oneSigmaBandColour)
    shadeBox.DrawBox(boxX1,boxY2,boxX2,boxY3)

    lumInFb = round(float(luminosity)/float(1000),2)
    self.drawLumiAndCMEVert(0.65,0.45,lumInFb,CME,0.04)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)
#    c.SaveAs(editableoutputname)

    if len(signals)==0:
      return
    elif len(signals)==1:
      signal = signals[0]
      obsLimits = self.calculateIntersectionOfGraphs(signal,observed,True,True)
      expLimits = self.calculateIntersectionOfGraphs(signal,expected1sigma,True,True)
      return [obsLimits,expLimits]
    else :
      output = []
      for signal in signals :
        obsLimits = self.calculateIntersectionOfGraphs(signal,observed,True,True)
        expLimits = self.calculateIntersectionOfGraphs(signal,expected1sigma,True,True)
        output.append([obsLimits,expLimits])
      return output

  def drawSeveralObservedAndExpected(self,observeds,expecteds1sigma,expecteds2sigma,legendnames,name,nameX,nameY,luminosity,CME,xmin,xmax,ymin,ymax,doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(0)
    c.SetLogy(1)
    c.SetGridx(0)
    c.SetGridy(0)

    # Set automatic axis range from graphs.
    # X axis range will be exactly ends of graphs
    xVals = []
    for i in range(observeds[0].GetN()) :
      xVals.append(observeds[0].GetX()[i])
    xVals.sort()
    if xmin == 'automatic':
      minX = xVals[-1]
    else :
      minX = xmin
    if xmax == 'automatic':
      maxX = xVals[0]
    else :
      maxX = xmax

    # Y axis range will be 3 orders of magnitude
    # above highest point of observed, because want space for legend
    # Lowest point should be 2 orders of magnitude below lowest point in observed
    if ymin == 'automatic' :
      minY = observeds[0].GetMinimum()/100
    else :
      minY = ymin
    if ymax == 'automatic' :
      maxY = observeds[0].GetMaximum()*1000
    else :
      maxY = ymax

    # Set axis names.
    # So far, should always be thus so don't pass as parameters.

    # Create legend
    leftOfLegend = 0.28
    bottomOfLegend = 0.68-0.05*(len(observeds)-1) # 0.64
    legend = self.makeLegend(leftOfLegend,bottomOfLegend,0.95,0.92) # 0.88

    # Set up display for expectations
    allerrorgraphs = expecteds1sigma+expecteds2sigma
    for graph in allerrorgraphs :
      graph.SetMarkerColor(1)
      graph.SetMarkerSize(1)
      graph.SetMarkerStyle(20)
      graph.SetLineColor(1)
      graph.SetLineWidth(3)
      graph.SetLineStyle(3)
      graph.GetXaxis().SetTitle(nameX)
      graph.GetYaxis().SetTitle(nameY)
      graph.GetXaxis().SetRangeUser(minX,maxX)
      graph.GetXaxis().SetNdivisions(705,ROOT.kTRUE)
      graph.GetYaxis().SetRangeUser(minY,maxY)
      if minX > 0.001 and minX < 1 :
        graph.GetXaxis().SetNoExponent(ROOT.kTRUE)

    for graph in expecteds1sigma :
      graph.SetFillColor(self.colourpalette.oneSigmaBandColour)
    for graph in expecteds2sigma :
      graph.SetFillColor(self.colourpalette.twoSigmaBandColour)

    # Set up display for observations
    for observed in observeds :
      index = observeds.index(observed)
      observed.SetMarkerColor(1)
      observed.SetMarkerSize(1)
#      observed.SetMarkerStyle(20)
      observed.SetMarkerStyle(24+index)
      observed.SetLineColor(1)
#      observed.SetLineWidth(3)
      observed.SetLineWidth(2)
      observed.SetLineStyle(1)
      observed.SetFillColor(0)
      observed.GetXaxis().SetTitle(nameX)
      observed.GetYaxis().SetTitle(nameY)
      observed.GetXaxis().SetRangeUser(minX,maxX)
      observed.GetYaxis().SetRangeUser(minY,maxY)
      if minX > 0.001 and minX < 1 :
        observed.GetXaxis().SetNoExponent(ROOT.kTRUE)

    # First one has to include axes or everything comes out blank
    # Rest have to NOT include axes or each successive one overwrites
    # previous. "SAME option does not exist for TGraph classes.
    expecteds1sigma[-1].Draw("A3")
    for graph in expecteds2sigma :
      graph.Draw("3") # no axis
    for graph in expecteds1sigma :
      graph.Draw("L3") # 1-sigma expectation error bands
#    for graph in expecteds1sigma :
#      graph.Draw("LX") # Center of expectation
    for observed in observeds :
      observed.Draw("PL") # Data points of measurement

    # Fill and draw legend
    for observed in observeds :
      index = observeds.index(observed)
      legend.AddEntry(observed,legendnames[index],"PL")#"L")
    legend.AddEntry(expecteds1sigma[0], "Expected 95% CL upper limits","L")
    legend.AddEntry( "NULL" , "68% and 95% bands","")

    self.drawATLASLabels(0.20,0.20)
    legend.Draw()

    shadeBox = ROOT.TBox()
    boxX1 = minX + (maxX - minX)*0.19 #21
    boxX2 = boxX1 + (maxX - minX)*0.125 # 135
    boxY1 = math.exp(math.log(minY) + (math.log(maxY) - math.log(minY))*(0.665-0.06*(len(observeds)-1))) #0.66
    boxY2 = math.exp(math.log(boxY1) + (math.log(maxY) - math.log(minY))*0.0155)
    boxY3 = math.exp(math.log(boxY2) + (math.log(maxY) - math.log(minY))*0.025)
    boxY4 = math.exp(math.log(boxY3) + (math.log(maxY) - math.log(minY))*0.0155)
    shadeBox.SetFillColor(self.colourpalette.twoSigmaBandColour)
    shadeBox.DrawBox(boxX1,boxY1,boxX2,boxY4)
    shadeBox.SetFillColor(self.colourpalette.oneSigmaBandColour)
    shadeBox.DrawBox(boxX1,boxY2,boxX2,boxY3)

    lumInFb = round(float(luminosity)/float(1000),2)
    lumiloc = min(0.45,bottomOfLegend-0.12)
    self.drawLumiAndCMEVert(0.65,lumiloc,lumInFb,CME,0.04)

    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)

  def drawSignalOverlaidOnBkgPlot(self,bkgPlot,signalPlots,signalMasses,legendlist,lumi,CME,yname,name,firstBin=-1,lastBin=-1,doLogY=False,printCanvas=True,doRectangular=False) :

    if (printCanvas) :
      canvasname = name+'_cv'
      outputname = name+'.eps'
      c = self.makeCanvas(canvasname,doRectangular)
      c.SetLogx(1)
      c.SetLogy(doLogY)
      c.SetGridx(0)
      c.SetGridy(0)

    # Use bin range within which bkgPlot has entries, 
    # plus one empty on either side if available
    lowbin,highbin = self.getAxisRangeFromHist(bkgPlot)
    if (firstBin>0) :
      lowbin=firstBin
    if (lastBin>0 and lastBin>=firstBin) :
      highbin = lastBin

    # Create legend
    if (printCanvas) :
      lumInFb = round(float(lumi)/float(1000),2)
      topOfLegend = 0.81
      widthOfRow = 0.05
      bottomOfLegend = topOfLegend-(widthOfRow * len(signalPlots))
      legend = self.makeLegend(0.2,bottomOfLegend,0.9,topOfLegend)

    # Calculate y axis range
    maxval = -10
    minval = 10
    for bin in range(bkgPlot.GetNbinsX()+1) :
      if bkgPlot.GetBinContent(bin) + bkgPlot.GetBinError(bin) > maxval :
        maxval = bkgPlot.GetBinContent(bin) + bkgPlot.GetBinError(bin)
      if bkgPlot.GetBinContent(bin) - bkgPlot.GetBinError(bin) < minval :
        minval = bkgPlot.GetBinContent(bin) - bkgPlot.GetBinError(bin)
    locationzero = (0.0 - minval)/(maxval - minval)
    cutoff = 0.50
    if (printCanvas) :
      cutoff = 0.35
    if locationzero > cutoff :
      maxval = (0.0 - minval)/cutoff + minval

    # If part of bigger plot, want axis labels to be near to some value such that labels easy to see
    if printCanvas :
      bkgPlot.GetYaxis().SetRangeUser(minval - 0.1,maxval+0.1)
    else :
      lowerint = math.floor(minval)
      if minval - lowerint > 0.7 : 
        minval = lowerint+0.5
      else :
        minval = lowerint - 0.3
      upperint = math.ceil(maxval)
      if upperint - maxval > 0.7 :
        maxval = upperint - 0.5
      else :
        maxval = upperint + 0.3
      bkgPlot.GetYaxis().SetRangeUser(minval,maxval)

    xname = "Reconstructed m_{jj} [GeV]"
    if (printCanvas) :
      self.drawDataHist(bkgPlot,lowbin,highbin,xname,yname,False,1,False)
    else :
      self.drawDataHist(bkgPlot,lowbin,highbin,xname,yname,False,3,False)
    if (printCanvas) :
      legend.AddEntry(bkgPlot,yname,"LP");

    goodcolours = self.getGoodColours(len(signalPlots))

    self.savegraphs = []
    for observed in signalPlots :

      index = signalPlots.index(observed)
      mass = signalMasses[index]
      newname = observed.GetName()+"_graph"
      plot = ROOT.TGraph()
      plot.SetName(newname)
      pointn = 0

      # Trim to only desired bins
      for bin in range(observed.GetNbinsX()+2) :
        if ((observed.GetBinLowEdge(bin)+observed.GetBinWidth(bin) < 0.7*mass) or\
           (observed.GetBinLowEdge(bin) > 1.2*mass)) :
          continue
        plot.SetPoint(pointn,observed.GetBinCenter(bin),observed.GetBinContent(bin))
        pointn = pointn+1

      plot.SetMarkerStyle(ROOT.kOpenCircle)
      plot.SetMarkerSize(1)
      plot.SetMarkerColor(goodcolours[index]) 

      plot.SetMarkerColor(goodcolours[index])
      plot.SetLineColor(goodcolours[index])
      plot.SetLineStyle(2);
      plot.GetXaxis().SetTitle("")
      plot.GetYaxis().SetTitle("")
      plot.SetTitle("")

      self.savegraphs.append(plot)

      if (printCanvas) :
        legend.AddEntry(self.savegraphs[index],legendlist[index],"LP");

      self.savegraphs[index].Draw("SAME CP")

    # Draw ratio plot once more to make sure it's on top
    bkgPlot.Draw("E SAME")

    if (printCanvas) :
      self.drawATLASLabels(0.22,0.88)
      self.drawCMEAndLumi(0.22,0.83,CME,lumInFb,0.04)
      legend.Draw()

      c.RedrawAxis()
      c.Update()
      c.SaveAs(outputname)

  def drawSignalOverlaidOnDataAndFit(self,dataHist,fitHist,signalPlots,signalMasses,legendlist,lumi,CME,yname,name,firstBin=-1,lastBin=-1,doLogX=False,doLogY=True,printCanvas=True,doRectangular=False,nPads = 1) :

    if (printCanvas) :
      canvasname = name+'_cv'
      outputname = name+'.eps'
      c = self.makeCanvas(canvasname,doRectangular)
      c.SetLogx(doLogX)
      c.SetLogy(doLogY)
    
    # Use bin range within which bkgPlot has entries, 
    # plus one empty on either side if available
    lowbin,highbin = self.getAxisRangeFromHist(dataHist)
    if (firstBin>0) :
      lowbin=firstBin
    if (lastBin>0 and lastBin>=firstBin) :
      highbin = lastBin

    # Create legend
    if (printCanvas) :
      lumInFb = round(float(lumi)/float(1000),2)
      if (doLogX) :
        topOfLegend = 0.5
        leftOfLegend = 0.2
      else :
        topOfLegend = 0.81
        leftOfLegend = 0.5
      widthOfRow = 0.05
      bottomOfLegend = topOfLegend-(widthOfRow*(len(signalPlots)+2))
      rightOfLegend = leftOfLegend+0.4
      legend = self.makeLegend(leftOfLegend,bottomOfLegend,rightOfLegend,topOfLegend)

    xname = "Reconstructed m_{jj} [GeV]"
    self.drawFitHist(fitHist,lowbin,highbin,xname,yname)

    goodcolours = self.getGoodColours(len(signalPlots))

    self.saveplots = []
    for observed in signalPlots :

      index = signalPlots.index(observed)

      mass = signalMasses[index]
      newname = observed.GetName()+"_graph_2"
      plot = ROOT.TGraph()
      plot.SetName(newname)
      pointn = 0

      # Trim to only desired bins
      for bin in range(observed.GetNbinsX()+2) :
        if ((observed.GetBinLowEdge(bin)+observed.GetBinWidth(bin) < 0.7*mass) or\
           (observed.GetBinLowEdge(bin) > 1.2*mass)) :
          continue
        plot.SetPoint(pointn,observed.GetBinCenter(bin),observed.GetBinContent(bin)+fitHist.GetBinContent(bin))
        pointn = pointn+1

      plot.SetMarkerStyle(ROOT.kOpenCircle)
      plot.SetMarkerSize(1)
      plot.SetMarkerColor(goodcolours[index])

      plot.SetMarkerColor(goodcolours[index])
      plot.SetLineColor(goodcolours[index])
      plot.SetLineStyle(2);
      plot.GetXaxis().SetTitle("")
      plot.GetYaxis().SetTitle("")
      plot.SetTitle("")

      self.saveplots.append(plot)
      if (printCanvas) :
        legend.AddEntry(self.saveplots[index],legendlist[index],"LP")

      self.saveplots[index].Draw("SAME CP")

    self.drawDataHist(dataHist,lowbin,highbin,"","",True,nPads)

    self.drawFitHist(fitHist,lowbin,highbin,xname,yname,True,True)

    if (printCanvas) :
      legend.AddEntry(dataHist,"Data","LP")
      legend.AddEntry(fitHist,"Fit","LF")
      if (doLogX) :
        self.drawATLASLabels(0.57, 0.85)
      else :
        self.drawATLASLabels(0.52, 0.88)
      self.drawCMEAndLumi(leftOfLegend+0.02,topOfLegend+0.02,CME,lumInFb,0.04)
      legend.Draw()
    
      c.RedrawAxis()
      c.Update()
      c.SaveAs(outputname)

  def drawSeveralObservedLimits(self,observedlist,signallegendlist,name,nameX,nameY,luminosity,CME,xmin,xmax,ymin,ymax,extraLegendLines = [], doLogY=True,doLogX=False,doRectangular=False,doLegendLocation="Right",ATLASLabelLocation="Bottom") :
    # LegendLocation should be "Right","Left", or "Wide"
    # ATLASLabelLocation should be "Bottom", "byLegend", or "None"
    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(doLogX)
    c.SetLogy(doLogY)
    c.SetGridx(0)
    c.SetGridy(0)

    # Set automatic axis range from graphs.
    # X axis range will be exactly ends of graphs
    xVals = []
    for list in observedlist :
      for i in range(list.GetN()) :
        xVals.append(list.GetX()[i])
    xVals.sort()
    if xmin == 'automatic':
      minX = xVals[0]
    else :
      minX = xmin
    if xmax == 'automatic':
      maxX = xVals[-1]
    else :
      maxX = xmax
    # Y axis range will be 2 orders of magnitude
    # above highest point of signal, because want space for legend
    # Lowest point should be 2 orders of magnitude below lowest point in observed
    if ymin == 'automatic' :
      minY = observedlist[0].GetMinimum()/100
    else :
      minY = ymin
    if ymax == 'automatic' :
      maxY = observedlist[-1].GetMaximum()*100
    else :
      maxY = ymax

    # Create legend
    if doLegendLocation=="Right" :
      leftOfAll = 0.60
    else :
       leftOfAll = 0.20
    topOfAll = 0.88
    widthOfRow = 0.05

    leftOfLegend = leftOfAll
    if doLegendLocation=="Left" :
      rightOfLegend = 0.50
    else :
      rightOfLegend = 0.90
    topOfLegend = topOfAll - (widthOfRow+0.01)*len(extraLegendLines)# 0.75
    if luminosity != [] and CME != [] :
      topOfLegend = topOfLegend - 0.09 
    bottomOfLegend = topOfLegend-(widthOfRow * len(observedlist))

    legend = self.makeLegend(leftOfLegend,bottomOfLegend,rightOfLegend,topOfLegend)

    # Set up display for expectations
    goodcolours = self.getGoodColours(len(observedlist))
    for observed in observedlist :

      index = observedlist.index(observed)
      colour = goodcolours[index]

      # Set up display for observations
      observed.SetMarkerColor(colour)
      observed.SetMarkerSize(0.7)  # was 0.5 back when things were nice
      observed.SetMarkerStyle(24+index) # was 20 when things were nice
      observed.SetLineColor(colour)
#      observed.SetLineWidth(3)
      observed.SetLineWidth(2)
      observed.SetLineStyle(1)
      observed.SetFillColor(0)
      observed.GetXaxis().SetTitle(nameX)
      observed.GetYaxis().SetTitle(nameY)
      observed.GetXaxis().SetLimits(minX,maxX)
      observed.GetYaxis().SetRangeUser(minY,maxY)
      observed.GetXaxis().SetNdivisions(605,ROOT.kTRUE)

      # First one has to include axes or everything comes out blank
      # Rest have to NOT include axes or each successive one overwrites
      # previous. "SAME option does not exist for TGraph classes.
      if index==0 :
        observed.Draw("APL") # Data points of measurement
      else :
        observed.Draw("PL")

      # Fill and draw legend
      legend.AddEntry(observed,signallegendlist[index],"PL")

    if (ATLASLabelLocation=="Bottom") :
      self.drawATLASLabels(0.20,0.20)
    elif (ATLASLabelLocation=="byLegend") :
      self.drawATLASLabels(leftOfLegend,bottomOfLegend-0.08)

    persistent = []
    if luminosity != [] and CME != [] :
      lumInFb = round(float(luminosity)/float(1000),2)
      persistent.append(self.drawCME(leftOfAll,topOfAll,CME,0.04))
      c.Update()
      persistent.append(self.drawLumi(leftOfAll,topOfAll-0.02-widthOfRow,lumInFb,0.04))

    c.Update()

    self.myLatex.SetTextFont(42)
    self.myLatex.SetTextSize(0.04)
    index = 0
    if len(extraLegendLines) > 0 :
      toplocation = topOfAll - (0.03+2*widthOfRow) - (0.01+widthOfRow)*(index)
      for line in extraLegendLines :
        toplocation = topOfAll - (0.03+2*widthOfRow) - (0.01+widthOfRow)*(index)
        persistent.append(self.myLatex.DrawLatex(leftOfLegend+0.01,toplocation,line))
        index = index+1

    if doLegendLocation=="Wide" :
      if len(observedlist) > 5 :
        legend.SetNColumns(2)
    legend.Draw()

    # Should have draw-box for the bands
    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)


  def drawPosteriorsWithCLs(self,posteriorsandclslist,legendlist,luminosity,CME,name,align=2,central=True,drawAsHist=False,addlinestolegend=False,doLogY=False,drawPriors=[],doRectangular=False) :

    canvasname = name+'_cv'
    outputname = name+'.eps'
    c = self.makeCanvas(canvasname,doRectangular)
    c.SetLogx(0)
    c.SetLogy(doLogY)
    c.SetGridx(0)
    c.SetGridy(0)

    # Set axis names.
    # So far, should always be thus so don't pass as parameters.
    nameX = "Number of signal events"
    nameY = "p(signal | data)"

    # Create legend
    ncomp = len(legendlist)
    y1forleg = 0.65 - 0.1*(ncomp-6)
    if y1forleg < 0 :
      y1forleg = 0

    legendsize = 0.04*(len(legendlist)+len(drawPriors))
    if addlinestolegend:
      legendsize = 2*legendsize
    if align == 0:
      leftOfLegend = 0.21
      rightOfLegend = 0.65
    else :
      leftOfLegend = 0.51
      rightOfLegend = 0.95
    if align == 1 :
      legendtop = 0.83
    else :
      legendtop = 0.76
    legendbottom = legendtop - legendsize
    legend = self.makeLegend(leftOfLegend,legendbottom,rightOfLegend,legendtop)

    # Set up display for expectations
    goodcolours = self.getGoodColours(len(posteriorsandclslist)+len(drawPriors))

    for observed in posteriorsandclslist :

      index = posteriorsandclslist.index(observed)
      colour = goodcolours[index]

      posterior = observed[0]
      cl = observed[1]

      # Set up display for observations
      posterior.SetMarkerColor(colour)
      posterior.SetMarkerSize(0.5)
      posterior.SetMarkerStyle(20)
      posterior.SetLineColor(colour)
      posterior.SetLineWidth(3)
      posterior.SetLineStyle(1)
      posterior.SetFillColor(0)
      posterior.GetXaxis().SetTitle(nameX)
      posterior.GetYaxis().SetTitle(nameY)

      if len(posteriorsandclslist) + len(drawPriors) < 3 :
        posterior.SetMarkerColor(1000)
        posterior.SetLineColor(1000)

      # Standard draw option:
      if index==0 :
        if posterior.GetMaximum() < 0.5 or posterior.GetMaximum > 1E4 :
          c.SetLeftMargin(0.175)
          posterior.GetYaxis().SetTitleOffset(1.8)
        if central == True:
          posterior.GetYaxis().SetRangeUser(0,1.8*posterior.GetMaximum())
        posterior.Draw("C")
      else :
        posterior.Draw("C SAME")

      # Draw line for CL in matching colour :
      clLine = ROOT.TLine()
      clLine.SetLineColor(colour)
      if len(posteriorsandclslist) + len(drawPriors) < 3 :
       clLine.SetLineColor(1000)
      lineMinY = 0
      lineMaxY = posterior.GetMaximum()/2
      if type(cl) is float :
        clLine.DrawLine(cl,lineMinY,cl,lineMaxY)
      else :
        clLine.DrawLine(cl[0],lineMinY,cl[0],lineMaxY)

      # Fill and draw legend
      legend.AddEntry(posterior,legendlist[index],"P")
      if addlinestolegend :
        legend.AddEntry(clLine,"95% quantile","L")

    colourindex = len(posteriorsandclslist)
    for prior in drawPriors :
      prior.SetLineWidth(3)
      prior.SetLineStyle(1)
      colour = goodcolours[colourindex]
      if len(posteriorsandclslist) + len(drawPriors) == 2 :
        colour = 1002
      prior.SetLineColor(colour)
      prior.SetFillColor(0)
      colourindex = colourindex+1
      legend.AddEntry(prior,"Prior","L")
      prior.Draw("C SAME")

    legend.Draw()
    lumInFb = round(float(luminosity)/float(1000),2)
    if align == 0 :
      self.drawATLASLabels(0.2, 0.87)
      self.drawCMEAndLumi(leftOfLegend+0.01,0.805,lumInFb,CME,0.04)
    elif align == 2 :
      self.drawATLASLabels(0.53, 0.87, True)
      self.drawCMEAndLumi(leftOfLegend+0.01,0.805,lumInFb,CME,0.04)
    else :
      self.drawATLASLabels(0.2,0.2)
      self.drawCMEAndLumi(leftOfLegend+0.01,0.87,lumInFb,CME,0.04)

    # Should have draw-box for the bands
    c.RedrawAxis()
    c.Update()
    c.SaveAs(outputname)


  ## ----------------------------------------------------
  ## Internal functions

  def makeCanvas(self,canvasname,doRectangular,doDoubleSize=False) :

    if doRectangular :
      if doDoubleSize :
        dim = 1600,600
      else :
        dim = 800,600
    else :
      if doDoubleSize :
        dim = 1200,600
      else :
        dim = 600,600
    return ROOT.TCanvas(canvasname,'',0,0,dim[0],dim[1])

  def makeLegend(self,legX1,legY1,legX2,legY2) :

    legend = ROOT.TLegend(legX1,legY1,legX2,legY2)
    legend.SetTextFont(42)
    legend.SetTextSize(0.04)
    legend.SetBorderSize(0)
    legend.SetLineColor(0)
    legend.SetLineStyle(1)
    legend.SetLineWidth(1)
    legend.SetFillColor(0)
    legend.SetFillStyle(1001)
    return legend

  def getAxisRangeFromHist(self,hist) :
    # Axis range should be decided by data hist
    firstBin =0
    while (hist.GetBinContent(firstBin+1)==0 and firstBin < hist.GetNbinsX()) :
      firstBin+=1
    lastBin = hist.GetNbinsX()+1
    while (hist.GetBinContent(lastBin-1)==0 and lastBin > 0) :
      lastBin-=1
    if (firstBin > lastBin) :
      firstBin=1
      lastBin = hist.GetNbinsX()
    return firstBin,lastBin

  def getYRangeFromHist(self,hist) :
    lowyval = 1E10
    lownonzero = 1E10
    highyval = -1E10
    lowbin,highbin = self.getAxisRangeFromHist(hist)
    for bin in range(lowbin,highbin) :
      if hist.GetBinContent(bin) < lowyval :
        lowyval = hist.GetBinContent(bin)
      if hist.GetBinContent(bin) < lownonzero and hist.GetBinContent(bin) > 0 :
        lownonzero = hist.GetBinContent(bin)
      if hist.GetBinContent(bin) > highyval :
        highyval = hist.GetBinContent(bin)
    return lowyval,lownonzero,highyval

  def getGoodColours(self, ncolours) :
    if ncolours < 4 :
      return self.colourpalette.shortGoodColours
    elif ncolours < 6 :
      return self.colourpalette.defaultGoodColours
    elif ncolours < 13 :
      return self.colourpalette.mediumGoodColours
    else :
      return self.colourpalette.longGoodColours

  def drawDataHist(self, dataHist,firstBin,lastBin,xname,yname,same=False,nPads=1,FixYAxis=False) :

    # Data hist must be in data points with weighted error bars
    dataHist.SetMarkerColor(ROOT.kBlack)
    dataHist.SetLineColor(ROOT.kBlack)
    dataHist.GetXaxis().SetTitle(xname)
    dataHist.GetYaxis().SetTitle(yname)
    dataHist.GetXaxis().SetRange(firstBin,lastBin)
    if FixYAxis :
      dataHist.GetYaxis().SetRangeUser(0.5,dataHist.GetBinContent(firstBin)*5)

    if nPads==2 :
      dataHist.GetYaxis().SetNdivisions(805,ROOT.kTRUE)
    elif nPads==1 :
      dataHist.GetYaxis().SetNdivisions(605,ROOT.kTRUE)
    elif nPads==3 :
      dataHist.GetYaxis().SetNdivisions(605,ROOT.kTRUE)
    if same :
      dataHist.Draw("E SAME")
    else :
      dataHist.Draw("E")

  def drawFitHist(self,fitHist,firstBin,lastBin,xname,yname,same=False,twoPads=False,useError=False) :

    # Fit hist must be expressed as a histo in a red line
    if (useError) :
      fitHist.SetLineColor(self.colourpalette.signalLineColours[0])
      fitHist.SetFillColor(self.colourpalette.signalErrorColours[0])
      fitHist.SetFillStyle(1001)
      fitHist.SetMarkerStyle(20)
      fitHist.SetMarkerSize(0.0)
    else :
      fitHist.SetLineColor(ROOT.kRed)
      fitHist.SetFillStyle(0)
    fitHist.SetLineWidth(2)
    fitHist.SetTitle("")
    fitHist.GetXaxis().SetTitle(xname)
    fitHist.GetYaxis().SetTitle(yname)
    fitHist.GetXaxis().SetRange(firstBin,lastBin)
    fitHist.GetYaxis().SetRangeUser(0.5,fitHist.GetBinContent(firstBin)*5)
    if twoPads :
      fitHist.GetXaxis().SetNdivisions(805,ROOT.kTRUE)
    else :
      fitHist.GetXaxis().SetNdivisions(605,ROOT.kTRUE)
    drawOption = "HIST ]["
    if useError :
      drawOption = "][ E3"
    if same :
      drawOption = drawOption + " SAME"
    fitHist.Draw(drawOption)

    # To get line on top need to redraw 
    persistent = fitHist.Clone("newone")
    persistent.SetFillStyle(0)
    if useError :
      persistent.SetLineColor(ROOT.kRed)
      persistent.Draw("][ L SAME")
    return persistent

  def drawSignificanceHist(self,significance,firstBin,lastBin,xname,yname,fixYAxis=False,inLargerPlot=False,doLogX=False,doErrors=False) :
    significance.SetLineColor(ROOT.kBlack)
    significance.SetLineWidth(2)
    significance.SetFillColor(ROOT.kRed)
    significance.SetFillStyle(1001)
    significance.SetTitle("")
    significance.GetXaxis().SetRange(firstBin,lastBin)
    lowPoint = significance.GetMaximum()
    highPoint = significance.GetMinimum()
    for bin in range(firstBin,lastBin+1) :
      val = significance.GetBinContent(bin)
      if val < lowPoint :
        lowPoint = val
      if val > highPoint :
        highPoint = val
    if highPoint == 20 :
      highPoint = 7
    if fixYAxis==False :
#      if (highPoint - lowPoint) > 2 :
#        significance.GetYaxis().SetRangeUser(lowPoint-0.5,highPoint+0.5)
      if lowPoint < 0 :
        significance.GetYaxis().SetRangeUser(lowPoint*1.2,max(highPoint*(1.2),0.15))
      else :
        significance.GetYaxis().SetRangeUser(lowPoint - 0.9*(highPoint - lowPoint),highPoint + 0.9*(highPoint - lowPoint))
    else :
      significance.GetYaxis().SetRangeUser(-3.7,3.7)
    if inLargerPlot :
      significance.GetYaxis().SetTickLength(0.055)
    significance.GetXaxis().SetNdivisions(805,ROOT.kTRUE)
    if doLogX :
      significance.GetXaxis().SetMoreLogLabels()
    significance.GetYaxis().SetTitle(yname)
    significance.GetXaxis().SetTitle(xname)
    if doErrors :
      significance.Draw("E")
    else :
      significance.Draw("HIST")

  def drawATLASLabels(self,xstart,ystart,rightalign=False,isRectangular=False) :
    self.myLatex.SetTextSize(0.05)
    self.myLatex.SetTextFont(72)
    extralength = {0:0, 1: 0, 2: -0.2, 3: 0.25, 4: 0.23, 5: -0.1, 6 : 0.12}
    if rightalign :
      xstart = xstart - extralength[self.labeltype]
    self.myLatex.DrawLatex(xstart, ystart, "ATLAS")
    if self.labeltype==0 :
      return
    spacing = 0.16
    if (isRectangular) :
      spacing = 0.13
    self.myLatex.SetTextFont(42)
    if self.labeltype==1 :
      self.myLatex.DrawLatex(xstart + spacing, ystart, "Preliminary")
    elif self.labeltype==2 :
      self.myLatex.DrawLatex(xstart + spacing, ystart, "Internal")
    elif self.labeltype==3 :
      self.myLatex.DrawLatex(xstart + spacing, ystart, "Simulation Preliminary")
    elif self.labeltype==4 :
      self.myLatex.DrawLatex(xstart + spacing, ystart, "Simulation Internal")
    elif self.labeltype==5 :
      self.myLatex.DrawLatex(xstart + spacing, ystart, "Simulation")
    elif self.labeltype==6 :
      self.myLatex.DrawLatex(xstart + spacing, ystart, "Work in Progress")
    return

  # Text height required is around 0.04 for size 0.04, 0.05 for size 0.05, etc

  def drawLumi(self,xstart,ystart,lumiInFb,fontsize=0.05) :
    self.whitebox.Clear()
    self.whitebox.SetTextSize(fontsize)
    self.whitebox.SetX1NDC(xstart-0.01)
    self.whitebox.SetY1NDC(ystart-0.01)
    self.whitebox.SetX2NDC(xstart+0.25)
    self.whitebox.SetY2NDC(ystart+0.06)
    self.whitebox.SetTextFont(42)
    mystring = "#scale[0.7]{#int}L dt"
    myfb = "fb^{-1}"
    self.whitebox.AddText(0.04,1.0/8.0,"{0} = {1} {2}".format(mystring,lumiInFb,myfb))
    persistent = self.whitebox.DrawClone()
    return persistent

  def drawCME(self,xstart,ystart,CME,fontsize=0.05) :
    self.whitebox.Clear()
    self.whitebox.SetTextSize(fontsize)
    self.whitebox.SetX1NDC(xstart-0.01)
    self.whitebox.SetY1NDC(ystart-0.01)
    self.whitebox.SetX2NDC(xstart+0.25)
    self.whitebox.SetY2NDC(ystart+0.05)
    mysqrt = "#sqrt{s}"
    self.whitebox.AddText(0.04,1.0/6.0,"{0}={1} TeV".format(mysqrt,CME))
    persistent = self.whitebox.DrawClone()
    return persistent

  def drawLumiAndCMEVert(self,xstart,ystart,lumiInFb,CME,fontsize=0.05) :
    self.whitebox.Clear()
    self.whitebox.SetFillColor(0)
    self.whitebox.SetFillStyle(1001)
    self.whitebox.SetTextColor(ROOT.kBlack)
    self.whitebox.SetTextFont(42)
    self.whitebox.SetTextAlign(11)
    self.whitebox.SetBorderSize(0)
    self.whitebox.SetTextSize(fontsize)
    self.whitebox.SetX1NDC(xstart-0.02)
    self.whitebox.SetY1NDC(ystart-0.01)
    self.whitebox.SetX2NDC(xstart+0.25)
    self.whitebox.SetY2NDC(ystart+0.11)
    mystring = "#scale[0.7]{#int}L dt"
    myfb = "fb^{-1}"
    inputstring1 = "{0} = {1} {2}".format(mystring,lumiInFb,myfb)
    self.whitebox.AddText(0.0,0.55,inputstring1)
    mysqrt = "#sqrt{s}"
    inputstring2 = "{0}={1} TeV".format(mysqrt,CME)
    self.whitebox.AddText(0.0,0.07,inputstring2)
    self.whitebox.Draw()
    return

  def drawCMEAndLumi(self,xstart,ystart,CME,lumiInFb,fontsize=0.05) :
    self.whitebox.Clear()
    self.whitebox.SetTextSize(fontsize)
    self.whitebox.SetX1NDC(xstart-0.01)
    self.whitebox.SetY1NDC(ystart-0.01)
    self.whitebox.SetX2NDC(xstart+0.40)
    self.whitebox.SetY2NDC(ystart+0.05)
    mysqrt = "#sqrt{s}"
    mystring = "#scale[0.7]{#int}L dt"
    myfb = "fb^{-1}"
    self.whitebox.AddText(0.04,1.0/8.0,"{0}={1} TeV, {2}={3} {4}".format(mysqrt,CME,mystring,lumiInFb,myfb))
    self.whitebox.Draw()
    return

  def drawXAxisInTeV(self,xmin,xmax,ymin,ymax,ndiv=510) :
    axisfunc = ROOT.TF1("axisfunc","x",float(xmin)/1000.0,float(xmax)/1000.0)
    newaxis = ROOT.TGaxis(xmin,ymin,xmax,ymax,"axisfunc",ndiv)
    return newaxis,axisfunc

  def calculateIntersectionOfGraphs(self, graph1, graph2, doLogGraph1=False, doLogGraph2=False) :

    crossings = []

    for point in range(graph1.GetN()-1) :
      graph1HigherAtLeft=False
      graph1HigherAtRight=False
      thisX1 = graph1.GetX()[point]
      thisX2 = graph1.GetX()[point+1]

      if graph1.Eval(thisX1) <= 0 or graph2.Eval(thisX1) <= 0 \
           or graph2.GetX()[0] > thisX1 or graph2.GetX()[graph2.GetN()-1] < thisX2 :
        continue

      graph1y1 = graph1.GetY()[point]
      graph1y2 = graph1.GetY()[point+1]
      if doLogGraph2 :
        graph2y1 = self.getGraphAtXWithLog(graph2,thisX1)
        graph2y2 = self.getGraphAtXWithLog(graph2,thisX2)
      else :
        graph2y1 = graph2.Eval(thisX1)
        graph2y2 = graph2.Eval(thisX2)

      if (graph1y1 > graph2y1) :
        graph1HigherAtLeft = True
      if (graph1y2 > graph2y2) :
        graph1HigherAtRight = True

      # If no crossing in this interval carry on
      if (graph1HigherAtLeft == graph1HigherAtRight) :
        continue

      # Otherwise, figure out where and keep it
      xtest = 0.5*(thisX1+thisX2)
      while(abs(thisX1-thisX2) > 0.001) :

        if doLogGraph1 :
          graph1y1 = self.getGraphAtXWithLog(graph1,thisX1)
          graph1y2 = self.getGraphAtXWithLog(graph1,thisX2)
          graph1ytest = self.getGraphAtXWithLog(graph1,xtest)
        else :
          graph1y1 = graph1.Eval(thisX1)
          graph1y2 = graph1.Eval(thisX2)
          graph1ytest = graph1.Eval(xtest)
        if doLogGraph2 :
          graph2y1 = self.getGraphAtXWithLog(graph2,thisX1)
          graph2y2 = self.getGraphAtXWithLog(graph2,thisX2)
          graph2ytest = self.getGraphAtXWithLog(graph2,xtest)
        else :
          graph2y1 = graph2.Eval(thisX1)
          graph2y2 = graph2.Eval(thisX2)
          graph2ytest = graph2.Eval(xtest)

        if ((graph1y1 >= graph2y1) and (graph1y2 <= graph2y2)) :
          if graph1ytest > graph2ytest :
            thisX1 = xtest
          else :
            thisX2 = xtest

        elif ((graph1y1 <= graph2y1) and (graph1y2 >= graph2y2)) :
          if graph1ytest > graph2ytest :
            thisX2 = xtest
          else :
            thisX1 = xtest
        xtest = 0.5*(thisX1+thisX2)

      crossings.append(xtest)

    return crossings

  def getGraphAtXWithLog(self, graph, x) :

    for point in range(graph.GetN()-1) :
      thisX1 = graph.GetX()[point]
      thisX2 = graph.GetX()[point+1]
      if thisX1 > x or thisX2 < x :
        continue
      thisY1 = graph.GetY()[point]
      thisY2 = graph.GetY()[point+1]
      m = (math.log(thisY2) - math.log(thisY1))/(thisX2 - thisX1)
      lny = math.log(thisY1) + m*(x-thisX1)
    return math.exp(lny)
